# GENOtation / Interpretome

As hosted on interpretome.stanford.edu

Make sure all changes made to site are commited to this git repository!

---------------------------------------
## Contents

[Installing Genotation on Mac OSX](#markdown-header-installing-genotation-on-mac-osx)

[Creating a new exercise in Genotation](#markdown-header-creating-a-new-exercise-in-genotation)

[Some basic git commands](#markdown-header-some-basic-git-commands)

---------------------------------------

# Installing Genotation on Mac OSX or Windows.

This will guide you through the steps to install and run Genotation and its dependencies on your Mac or Windows computer.

## Install an IDE

###For Mac:

If you don't already have it, you'll need to install Apple's [Xcode](https://developer.apple.com/xcode/downloads/).

Make sure you install the Xcode command line tools. Open Xcode, go to Preferences > Downloads, and click the button to DL the command line tools.

###For Windows:

Install your favorite IDE.  For Python, you may consider [PyCharm](https://www.jetbrains.com/pycharm/download/#section=windows).

## Make sure you're running Python 2.7.x

###For Mac/Linux:

Open Terminal and type:

    $ python -V

The version of Python will show.

Note:

- Python comes pre-installed on OSX
- If you have a recent version of OSX (>=10.7) it probably has 2.7 installed already. Note 2.6 will probably work as well, though not tested.

If Python 2.7 (or MAYBE 2.6) is not installed or is not showing as the default Python ... ask a professional for help. :)

###For Windows:

Go to [https://www.python.org/downloads/release/python-2710/](https://www.python.org/downloads/release/python-2710/).  Click on the "Windows x86-64 MSI installer"

When the python-2.7.10.amd64.msi file (or something similar) has finished downloading, click on it to open.  Follow the directions:

1.  It's OK to select "For all users". 
2.  When presented with a list that look like many folders, the very last one will say something like "Add to Path" and will have a red X on the icon.  Click on that icon and select "Run from Disk" (or something similar).

## Install Git (version control system)

Download the latest version of git: [http://sourceforge.net/projects/git-osx-installer/files/](http://sourceforge.net/projects/git-osx-installer/files/)

Git keeps track of all changes made to the software.

## Install MySQL

###For Mac/Linux:

Download: [http://dev.mysql.com/downloads/mysql/](http://dev.mysql.com/downloads/mysql/)

- Download and run the installer for MySQL 5.6.x - 64-bit DMG Archive version. 
- Open the disk image and first run the installer, e.g. `mysql-5.6.16-osx10.7-x86_64.dmg`
- Run `MySQLStartupItem.pkg`
- Finally, double click the `MySQL.prefpane` file to install that into your system preferences.
- Open system preferences: [Apple] > System Preferences..., click the MySQL icon near the bottom and click the button to start MySQL

Open a terminal window.  Type "mysql".  If you get an error that says "mysql is not found", you may want to place mysql on your path.
To do so, follow the instruction found at [http://coolestguidesontheplanet.com/add-shell-path-osx/](http://coolestguidesontheplanet.com/add-shell-path-osx/).  In short:

- Go to your home directory by typing: cd
- Type: nano .bash_profile
- Type: export PATH="/usr/local/mysql/bin:$PATH"
- Hit control-o to write the file.
- Hit control-x to exit the editor.
- Close the terminal window and re-open a new one.

###For Windows:

Go to [http://dev.mysql.com/downloads/windows/installer/5.7.html](http://dev.mysql.com/downloads/windows/installer/5.7.html).  Click on the Windows MSI Installed "Download" button. 

When the mysql-installer-community-5.7.11.0.msi file (or something similar) has finished downloading, click on it to open.  Follow the directions.

Leave the checkmark that says "Run as a Windows service".  If you uncheck the box "Start automatically when Windows start", you'll need to manually start MySQL before using it.  Instructions to do so are below.

The installer will copy files in the "C:\Program Files\MySQL" and the "C:\ProgramData\MySQL\MySQL Server 5.7" directories.  The latter is not always visible in Explorer but if you start typing "C:\ProgramData" in the text bar, you should be able to access it.

Copy the file "C:\ProgramData\MySQL\MySQL Server 5.7\my.ini" to "C:\Program Files\MySQL\MySQL Server 5.7\my.ini".

When you want to start MySQL, do:

1. At the Start menu, type "cmd".  
2. Right click on the icon that shows up.  It looks like a black rectangle.  Select "Run as administrator".  Click OK on any prompt.
3. Type "net start MySQL57".  The last two digits depend on the version of MySQL you have installed.
4. Type "mysqld".
5. That should be it.  To stop it, in the same window, type "net stop MySQL57" or shut down Windows.

## Create MySQL database user for Genotation

Open Terminal and use MySQL as root. Type:
    
    $ mysql -u root

If your root has a password, use:

    $ mysql -u root -p

Add user 'tomadmin' and assign priveleges (you'll need to ask Stuart or a previous TA for the 'password'):

    mysql> CREATE USER 'tomadmin'@'localhost' IDENTIFIED BY 'password';
    mysql> GRANT ALL PRIVILEGES ON *.* TO 'tomadmin'@'localhost'
        ->     WITH GRANT OPTION;

For security, you may want to set a password for the root user, anonymous users, and/or create a other user accounts. See references.

**Reference:**

- [http://dev.mysql.com/doc/refman/5.6/en/default-privileges.html](http://dev.mysql.com/doc/refman/5.6/en/default-privileges.html)
- [http://dev.mysql.com/doc/refman/5.6/en/adding-users.html](http://dev.mysql.com/doc/refman/5.6/en/adding-users.html)

## Get copies of the databases and create them locally

To run Genotation, you'll need a basic set of class exercises and a list of snps. Retrieve these two databases (dbs) from the live website. Open Terminal and 'cd' to a directory you want to download the db files into. Then get the files (you'll need the password for user 'tomadmin'):

    $ mysqldump -u tomadmin -p -h interpretome.stanford.edu interpretome_exercises > interpretome_exercises.sql
    $ mysqldump -u tomadmin -p -h interpretome.stanford.edu var_dbsnp > var_dbsnp.sql

Now create the databases locally and load the data. First, get to the MySQL command prompt (`$ mysql -u root`):

    mysql> CREATE DATABASE interpretome_exercises;
    mysql> CREATE DATABASE var_dbsnp;
    mysql> exit
    
And now load the data from the Terminal prompt:

    $ mysql -u tomadmin -p  interpretome_exercises < interpretome_exercises.sql
    $ mysql -u tomadmin -p  var_dbsnp < var_dbsnp.sql

## Clone Genotation code from Bitbucket

The code is stored (via git) on a source code hosting service called Bitbucket (similar to Github): [https://bitbucket.org/GregRR/genotation/overview](https://bitbucket.org/GregRR/genotation/overview)

If you are going to be editing/adding to the code you will need to create a Bitbucket account and have one of the admins add you to the repo.

Using Terminal, navigate to where you want to store the Genotation code (e.g., your Sites folder on Mac) and download the code using git:

    $ git clone https://YOUR_USER_NAME@bitbucket.org/GregRR/genotation.git

Or:

    $ git clone https://YOUR_USER_NAME@bitbucket.org/stuartkim/genotation.git

Replace YOUR_USER_NAME with your bitbucket user name, or you can copy and paste this URL from the HTTPS field at the top right of this page ([overview page](https://bitbucket.org/GregRR/genotation/overview)).

## Install pip
[pip](http://www.pip-installer.org/en/latest/quickstart.html) is a Python package manager. 

Download and save this file: [get-pip.py](https://raw.github.com/pypa/pip/master/contrib/get-pip.py)

In Terminal, find the file and run it using: 

     $ python get-pip.py

Reference: [http://www.pip-installer.org/en/latest/installing.html](http://www.pip-installer.org/en/latest/installing.html)

## Install VirtualEnv

[virtualenv](http://www.virtualenv.org/en/latest/virtualenv.html) is a tool to create isolated Python environments.

From Terminal, run these commands replacing X.X with the latest version number (currently 1.11.4):

    $ curl -O https://pypi.python.org/packages/source/v/virtualenv/virtualenv-X.X.tar.gz
    $ tar xvfz virtualenv-X.X.tar.gz
    $ cd virtualenv-X.X
    $ [sudo] python setup.py install
    
Or install virtualenv using pip (use sudo if needed):

    $ [sudo] pip install virtualenv

VirtualEnv reference: [http://www.virtualenv.org/en/latest/virtualenv.html](http://www.virtualenv.org/en/latest/virtualenv.html)

## Create a VirtualEnv

Now create a virtual environment to run Genotation in. In the same directory that you cloned Genotation to (but NOT IN the Genotation source code directory):

    $ virtualenv genotationEnv

Now start the virtualenv:

    $ cd genotationEnv
    $ source bin/activate

For PC Users:

    $ cd genotationEnv
    $ Scripts\activate.bat

Notice your command prompt now starts with `(genotationEnv)`. You will need to start this virtualenv each time you want to run the application. 

## Install the Django web app framework and Genotation Python dependancies:

    $ pip install django==1.3
    $ pip install mysql-python
    $ pip install numpy
    
Make sure you are installing those in your virtualenv. And make sure to install v1.3 of [Django](https://docs.djangoproject.com/en/1.3/).

### mysql-python note: make sure your virtualenv can find mysql_config

When installing mysql-python your virtualenv may not be able to find the file mysql_config. If this is the case add the THIRD line here to this sectionof your virtualenv's `activate` (not activate.py) file:
    
    _OLD_VIRTUAL_PATH="$PATH"
    PATH="$VIRTUAL_ENV/bin:$PATH"
    PATH="$PATH:/usr/local/mysql/bin/"  #add this line!
    export PATH

For details see this blog post: [virtualenv – EnvironmentError: mysql_config not found](http://mdshaonimran.wordpress.com/2012/01/14/virtualenv-environmenterror-mysql_config-not-found/)

## Configure the app to run on your computer

Open interpretome/www-data/interpretome/settings.py and change this line to point to the /media/ directory on your machine:

    STATICFILES_DIRS = ('/Users/your_name/path/to/Genotation/interpretome/www-data/interpretome/media/',)

You make also need to make a symbolic link to `libmysqlclient.18.dylib` in order for Django to find it. If you try running the app and get an error about this file, and it is not located in /usr/lib/ create a symbolic link:

    $ ln -s /usr/local/mysql/lib/libmysqlclient.18.dylib /usr/lib/libmysqlclient.18.dylib
    
For details see this [StackOverflow question](http://stackoverflow.com/questions/15593087/django-core-exceptions-improperlyconfigured-error-loading-mysqldb-module).

## Start Genotation

From Terminal, cd into the Genotation source code up to the directory containing the file: `manage.py` (interpretome/www-data/interpretome/manage.py).

Start the app:

    $ python manage.py runserver

Converseley you can stay in the root dir and do something like:

    $ python interpretome/www-data/interpretome/manage.py runserver

You'll then see that the app is running:


    Validating models...
    
    0 errors found
    Django version 1.3.1, using settings 'interpretome.settings'
    Development server is running at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.

In a web browser (Firefox or Chrome) enter the url: http://127.0.0.1:8000/. You should see the Genotation home page.

---------------------------------------

# Creating a new exercise in Genotation

Coming soon...

For now see this document: [Genotation Overview.docx](https://drive.google.com/file/d/0B0cfkWKVZOG9NDVCNG9nc0pTZ1U/edit?usp=sharing)

---------------------------------------

# Some basic git commands

##General
`git init` do this in the root dir of an app to make it a git repository (note, this is already done when you clone this code)

`git clone <url_of_repository>` clones an existing remote git repo (from github or bitbucket) onto your machine


##Viewing and Commiting Changes on you local copy
`git status` shows files that you have changes, what's been staged for commit (with git add), etc.

`git add <filename>` -or- `git add .`  to stage files for commit. 
'git rm --cached [filename]'  to remove files from tracking

`git commit -m "<your commit msg>" <file_name>`   commits your changes changes. LIke saving a file to go back to.  Message should be informative and specific.

'git diff <file>'' check the changes since the last commit.

`git push` push committed changes to remote repository (i.e. BitBucket)

'git log' tells history of project.


##Getting changes from remote repo

`git fetch`

`git pull --rebase` pulls new changes from remote repository to your local one

`git checkout <filename>`  checks out file from remote repo and **overwrites** your local copy

See more commands on the [git website](http://git-scm.com/docs/gittutorial).