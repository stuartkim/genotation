INSERT INTO kim_linkage(dbsnp, type, info_url) VALUES("rs6447271", "unlinked", "http://genome.ucsc.edu/cgi-bin/hgc?c=chr4&o=43438918&t=43438919&g=snp137&i=rs6447271");

INSERT INTO kim_linkage(dbsnp, type, info_url) VALUES("rs12426597", "unlinked", "http://genome.ucsc.edu/cgi-bin/hgc?c=chr12&o=67026184&t=67026185&g=snp137&i=rs12426597");

INSERT INTO kim_linkage(dbsnp, type, info_url) VALUES("rs878198", "unlinked", "http://genome.ucsc.edu/cgi-bin/hgc?c=chr20&o=47179652&t=47179653&g=snp137&i=rs878198");



INSERT INTO kim_linkage(dbsnp, type, info_url) VALUES("rs1333049", "linked", "http://genome.ucsc.edu/cgi-bin/hgc?c=chr9&o=22125502&t=22125503&g=snp137&i=rs1333049");

INSERT INTO kim_linkage(dbsnp, type, info_url) VALUES("rs10757274", "linked", "genome.ucsc.edu/cgi-bin/hgc?c=chr9&o=22096054&t=22096055&g=snp137&i=rs10757274");

INSERT INTO kim_linkage(dbsnp, type, info_url) VALUES("rs1537375", "linked", "http://genome.ucsc.edu/cgi-bin/hgc?c=chr9&o=22116070&t=22116071&g=snp137&i=rs1537375");



INSERT INTO kim_linkage(dbsnp, type, info_url) VALUES("rs7495174", "trait: eye color", "http://genome.ucsc.edu/cgi-bin/hgc?c=chr15&o=28344237&t=28344238&g=snp137&i=rs7495174");

INSERT INTO kim_linkage(dbsnp, type, info_url) VALUES("rs17822931", "trait: ear wax", "http://genome.ucsc.edu/cgi-bin/hgc?c=chr16&o=48258197&t=48258198&g=snp137&i=rs17822931");

INSERT INTO kim_linkage(dbsnp, type, info_url) VALUES("rs4988235", "trait: lactose", "http://genome.ucsc.edu/cgi-bin/hgc?c=chr2&o=136608645&t=136608646&g=snp137&i=rs4988235");