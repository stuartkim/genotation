 create kim_linkage table

CREATE TABLE kim_linkage(
	id bigint(20) unsigned NOT NULL auto_increment,
	dbsnp varchar(20) default NULL UNIQUE,
	ref varchar(1) default NULL,
	maf float default NULL,
	info_url varchar(512) default NULL,
	PRIMARY KEY (id),
	UNIQUE (dbsnp)
);