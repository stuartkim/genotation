euro <- read.table("CAD_EURO_RESULTS.txt", sep="\t", header=TRUE)
asia <- read.table("CAD_ASIA_RESULTS.txt", sep="\t", header=TRUE)

europop <- read.table("Population_distributions/CADWHriskscore_nocenter.txt", sep="\t", header=TRUE)
asiapop <- read.table("Population_distributions/EACD_ScoreHM.txt", sep="\t", header=TRUE)

pdf("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/CAD_euro.pdf")
attach(europop)
hist(scores,probability=TRUE, xlim=c(-1.5,1.5), ylim=c(0,1.25), xlab="Risk Scores", ylab="Density", main = "CAD Risk Scores - Europeans")
legend("topright",bty="n", pch=c("-","-"),col=c("black","red"),legend=c("CEU HapMap","Class Individuals"))
par(new=TRUE)
attach(euro)
for (i in scores){
	abline(v=scores[i], col = "red", lty=2)
}
dev.off()

pdf("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/CAD_asia.pdf")
attach(asiapop)
hist(scores,probability=TRUE, xlim=c(-1.5,1), ylim=c(0,1.25), xlab="Risk Scores", ylab="Density", main = "CAD Risk Scores - Asians")
legend("topright",bty="n", pch=c("-","-"),col=c("black","red"),legend=c("CHB+JPT HapMap","Class Individuals"))
par(new=TRUE)
attach(asia)
for (i in scores){
	abline(v=scores[i], col = "red", lty=2)
}
dev.off()
