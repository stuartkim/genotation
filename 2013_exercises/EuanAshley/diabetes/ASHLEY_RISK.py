"""

"""
from __future__ import division
import math
import sys
import os
import MySQLdb



db = MySQLdb.connect(host="fulltext.stanford.edu", port=3306, user="tomadmin", passwd="ooze4PhD", db="interpretome_exercises")
c = db.cursor()

query = """
select *
from unified where exercise = 'ashley_diabetes'
"""

c.execute(query)

data = c.fetchall()

SNPLIST = open('DM_SNPs_addpop.txt','r')

snp_population = dict()
eurosnp = dict()
asiasnp = dict()

for line in SNPLIST:
	fields = line.rstrip().split("\t")
	snp = fields[0]
	risk = fields[1]
	oddsratio = fields[5]
	population = fields[6]
	if snp == 'snp':
		continue
	if population == 'ASIA':
		snp_population[snp] = 'ASIA';
		asiasnp[snp] = dict()
		asiasnp[snp]['oddsratio'] = float(oddsratio)
		asiasnp[snp]['risk'] = risk
	elif population == 'EURO':
		snp_population[snp] = 'EURO';
		eurosnp[snp] = dict()
		eurosnp[snp]['oddsratio'] = float(oddsratio)
		eurosnp[snp]['risk'] = risk  

euroscore = dict()
asiascore = dict()

for ID, time, snp, genotype, population, person, exercise in data:
	if population == 'CEU' and snp in eurosnp:
		numrisk = genotype.count(eurosnp[snp]['risk'])
		if person in euroscore:
			euroscore[person] = euroscore[person] + numrisk * math.log(eurosnp[snp]['oddsratio'])
		else:
			euroscore[person] = numrisk * math.log(eurosnp[snp]['oddsratio'])
	elif population == ('CHB' or 'JPT') and snp in asiasnp:
		numrisk = genotype.count(asiasnp[snp]['risk'])
		if person in asiascore:
			asiascore[person] = asiascore[person] + numrisk * math.log(asiasnp[snp]['oddsratio'])
		else:
			asiascore[person] = numrisk * math.log(asiasnp[snp]['oddsratio'])

euro_scores = open('DM_EURO_RESULTS.txt','w')
asia_scores = open('DM_ASIA_RESULTS.txt','w')

print >> euro_scores, 'scores'
print >> asia_scores, 'scores'

for person in euroscore:
	print >> euro_scores, euroscore[person]

for person in asiascore:
	print >> asia_scores, asiascore[person]
	
