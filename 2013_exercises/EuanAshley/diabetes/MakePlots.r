euro <- read.table("DM_EURO_RESULTS.txt", sep="\t", header=TRUE)
asia <- read.table("DM_ASIA_RESULTS.txt", sep="\t", header=TRUE)

europop <- read.table("../Population_distributions/WHDM_HM_score.txt", sep="\t", header=TRUE)
asiapop <- read.table("../Population_distributions/EADM_ScoreHM.txt", sep="\t", header=TRUE)

pdf("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/DM_euro.pdf")
attach(europop)
hist(scores,probability=TRUE, xlim=c(1,4.5), ylim=c(0,1.25), xlab="Risk Scores", ylab="Density", main = "Diabetes Risk Scores - Europeans")
legend("topright",bty="n", pch=c("-","-"),col=c("black","red"),legend=c("CEU HapMap","Class Individuals"))
par(new=TRUE)
detach(europop)
attach(euro)
for (i in scores){
	abline(v=scores[i], col = "red", lty=2)
}
dev.off()

pdf("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/DM_asia.pdf")
detach(euro)
attach(asiapop)
hist(scores,probability=TRUE, xlim=c(-1,1), ylim=c(0,1.4), xlab="Risk Scores", ylab="Density", main = "Diabetes Risk Scores - Asians")
legend("topright",bty="n", pch=c("-","-"),col=c("black","red"),legend=c("CHB+JPT HapMap","Class Individuals"))
par(new=TRUE)
detach(asiapop)
attach(asia)
for (i in scores){
	abline(v=scores[i], col = "red", lty=2)
}
dev.off()
