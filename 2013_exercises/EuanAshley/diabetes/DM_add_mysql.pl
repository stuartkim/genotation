use warnings;
use strict;

open (my $INPUT, "<", "DM_SNPS.txt");

open (my $OUTPUT, ">", "DM_mysql_insert.txt");

while (<$INPUT>) {
	chomp;
	my @fields = split(/\t/,$_);
	my ($dbsnp, $risk_allele, $gene, $risk_freq, $OR,$pop) = ($fields[0],$fields[1],$fields[3],$fields[4],$fields[5],$fields[6]);
	if ($risk_freq) {
		print $OUTPUT "insert into ashley\_diabetes \(dbsnp\,genes\,risk\_frequency\,risk\_allele\,combined\_or\,pop) values \(\'$dbsnp\'\,\'$gene\'\,\'$risk_freq\'\,\'$risk_allele\'\,$OR\,\'$pop\'\)\;\n";
	} else {
		print $OUTPUT "insert into ashley\_diabetes \(dbsnp\,genes\,risk\_frequency\,risk\_allele\,combined\_or\,pop) values \(\'$dbsnp\'\,\'$gene\'\,\'N\/A\'\,\'$risk_allele\'\,$OR\,\'$pop\'\)\;\n";
	}
}
close $INPUT;
close $OUTPUT;
