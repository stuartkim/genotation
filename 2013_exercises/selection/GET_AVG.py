"""

"""

import sys
import os
import MySQLdb



db = MySQLdb.connect(host="fulltext.stanford.edu", port=3306, user="tomadmin", passwd="ooze4PhD", db="interpretome_exercises")
c = db.cursor()

query = """
select *
from selection
"""

c.execute(query)

data = c.fetchall()

euroavg = 0.0;
asianavg = 0.0;
africanavg = 0.0;

euronum = 0.0;
asiannum = 0.0;
africannum = 0.0;

for ID, SNP, anc, sele, freq, pop, gene in data:
	if freq:
		if pop == 'CEU':
			euroavg = euroavg + float(freq)
			euronum = euronum + 1
		elif pop == 'CHB':
			asianavg = asianavg + float(freq)
			asiannum = asiannum + 1
		elif pop == 'YRI':
			africanavg = africanavg + float(freq)
			africannum = africannum + 1

eurosum = euroavg/euronum
asiansum = asianavg/asiannum
africansum = africanavg/africannum

print eurosum
print asiansum
print africansum	
