"""

"""

import sys
import os
import MySQLdb



db = MySQLdb.connect(host="fulltext.stanford.edu", port=3306, user="tomadmin", passwd="ooze4PhD", db="interpretome_exercises")
c = db.cursor()

query = """
select *
from unified where exercise = 'selection' union select * from unified_2012 where exercise = 'selection'
"""

c.execute(query)

data = c.fetchall()

european = open('Selection_european.txt','w')
chinese = open('Selection_chinese.txt','w')
african = open('Selection_african.txt','w')

print >> european, 'fraction'
print >> chinese, 'fraction'
print >> african, 'fraction'

for ID, time, key, value, population, person, exercise in data:
	if key == 'stuart_cant_divide':
		if population == 'CEU':
			print >> european, value
		elif population == 'CHB' or population == 'JPT':
			print >> chinese, value
		elif population == 'YRI':
			print >> african, value		
