use warnings;
use strict;

open (my $INPUT, "<", "Selection_SelectedFreq.txt");
open (my $OUTPUT, ">", "Selection_modifymysql.txt");

while (<$INPUT>) {
	chomp;
	my @fields = split;
	my ($rsid, $freq, $pop) = @fields;
	next if ($rsid eq 'rsid');
	print $OUTPUT "update selection set selected_freq = $freq where dbsnp=$rsid and pop=\'$pop\';\n";
}
close $INPUT;
close $OUTPUT;
