"""

"""


import sys
import os
import MySQLdb



db = MySQLdb.connect(host="fulltext.stanford.edu", port=3306, user="tomadmin", passwd="ooze4PhD", db="interpretome_exercises")
c = db.cursor()

query = """
select *
from selection
"""

c.execute(query)

data = c.fetchall()

neandertal = dict()

frequency = open('Selection_SelectedFreq.txt','w')

for ID, SNP, ancestral, selected, pop, genes in data:
	if pop == 'CEU':
		database = 'allele_freqs_CEU'
	elif pop == 'YRI':
		database = 'allele_freqs_YRI'
	elif pop == 'CHB':
		database = 'allele_freqs_CHB'
	elif pop == 'JPT':
		database = 'allele_freqs_JPT'

	db2 = MySQLdb.connect(host="fulltext.stanford.edu", port=3306, user="tomadmin", passwd="ooze4PhD", db="var_hapmap")
	c2 = db2.cursor()
	query2 = """
	select * from %s where rsid = %s
	""" % (database, SNP)
	
	c2.execute(query2)
	data2 = c2.fetchall()

	for rsid, chrom, pos, strand, build, center, protlsid, assaylsid, panellsid, qc_code, refallele, refallele_freq, refallele_count, otherallele, otherallele_freq, otherallele_count, totalcount in data2:
		if (selected == refallele or selected == otherallele):
			if selected == refallele:
				selected_allele_freq = refallele_freq
			else:
				selected_allele_freq = otherallele_freq
		else:
			if selected == 'A':
				selected = 'T'
			if selected == 'T':
				selected = 'A'
			if selected == 'C':
				selected = 'G'
			if selected == 'G':
				selected = 'C'
			if selected == refallele:
				selected_allele_freq = refallele_freq
			else:
				selected_allele_freq = otherallele_freq
		print >> frequency, "%s\t%s\t%s" % (SNP, selected_allele_freq, pop)				
