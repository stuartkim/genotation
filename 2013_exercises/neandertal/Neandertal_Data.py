"""

"""

#sed s/"%"//g Neandertal_ClassData.txt > Neandertal_ClassData_fix.txt

import sys
import os
import MySQLdb



db = MySQLdb.connect(host="fulltext.stanford.edu", port=3306, user="tomadmin", passwd="ooze4PhD", db="interpretome_exercises")
c = db.cursor()

query = """
select *
from unified where exercise = 'neandertal'
"""

c.execute(query)

data = c.fetchall()

neandertal = dict()

for ID, time, key, value, population, person, exercise in data:
	if person in neandertal:
		if key == 'total_neandertal_index':
			neandertal[person]['totalAlleles'] = value
		elif key == 'neandertal_23andMe':
			neandertal[person]['23andme'] = value
	else:
		neandertal[person] = dict()
		if key == 'total_neandertal_index':
			neandertal[person]['totalAlleles'] = value
		elif key == 'neandertal_23andMe':
			neandertal[person]['23andme'] = value

class_data = open('Neandertal_ClassData.txt','w')
print >> class_data, "totalAlleles\tCompanyScore"

for person in neandertal:
	if neandertal[person]['23andme']:
		print >> class_data, "%s\t%s" % (neandertal[person]['totalAlleles'], neandertal[person]['23andme'])
	
