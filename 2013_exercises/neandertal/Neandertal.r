
neandertal <- read.table("Neandertal_ClassData_fix.txt", sep="\t", header=TRUE)
attach(neandertal)

regr = lm(CompanyScore~totalAlleles)
png("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/neandertal_scatterplot.png")
plot(totalAlleles,CompanyScore, pch=21,cex.main=1, bg="black",xlim = c(0,20), ylim = c(0,5), xlab= "Number of neandertal alleles", ylab = "23andme neandertal percentage", main = paste("Neandertal ancestry methods comparison, correlation=", format(summary(regr)$adj.r.squared,digits=3)))
abline(regr, col="red")
dev.off()

png("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/neandertal_bargraph.png")
hist(totalAlleles, xlab = "Number of neandertal alleles", ylab = "Number of individuals", xlim = c(0,20), breaks = c(0:20), main = "How neandertal are you?")
dev.off() 
