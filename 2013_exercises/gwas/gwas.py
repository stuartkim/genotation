"""

"""
from __future__ import division
import sys
import os
import MySQLdb



db = MySQLdb.connect(host="fulltext.stanford.edu", port=3306, user="tomadmin", passwd="ooze4PhD", db="interpretome_exercises")
c = db.cursor()

query = """
select *
from class_gwas
where `4988235` is not null
"""

#query = """
#select *
#from class_gwas
#where `4988235` is not null
#"""
c.execute(query)

data = c.fetchall()

snps = [('A', 'G'),
        ('A', 'G'),
        ('C', 'G'),
        ('C', 'T'),
        ('A', 'G')]

genos = [('AA', 'AG', 'GA', 'GG'),
        ('AA', 'AG', 'GA', 'GG'),
        ('CC', 'CG', 'GC', 'GG'),
        ('CC', 'CT', 'TC', 'TT'),
        ('AA', 'AG', 'GA', 'GG')]

snp_name = ['4988235', '7495174', '713598', '17822931', '4481887']

phenotypes = ['earwax', 'eyes', 'asparagus', 'bitter', 'lactose']

traits = [  ('Wet', "Dry"),
            ('Brown/Other', 'Blue/Green'),
            ('Can Smell', 'Can\'t Smell'),
            ('Can Taste', 'Can\'t Taste'),
            ('Intolerant', 'Tolerant')]

gwas = dict()
geno1 = dict()
geno2 = dict()

for snp in snp_name:
    gwas[snp] = dict()
    gwas[snp]['earwax'] = [0, 0, 0, 0]
    gwas[snp]['eyes'] = [0, 0, 0, 0]
    gwas[snp]['asparagus'] = [0, 0, 0, 0]
    gwas[snp]['bitter'] = [0, 0, 0, 0]
    gwas[snp]['lactose'] = [0, 0, 0, 0]
    geno1[snp] = dict()
    geno1[snp]['earwax'] = [0, 0, 0, 0]
    geno1[snp]['eyes'] = [0, 0, 0, 0]
    geno1[snp]['asparagus'] = [0, 0, 0, 0]
    geno1[snp]['bitter'] = [0, 0, 0, 0]
    geno1[snp]['lactose'] = [0, 0, 0, 0]
    geno2[snp] = dict()
    geno2[snp]['earwax'] = [0, 0, 0, 0]
    geno2[snp]['eyes'] = [0, 0, 0, 0]
    geno2[snp]['asparagus'] = [0, 0, 0, 0]
    geno2[snp]['bitter'] = [0, 0, 0, 0]
    geno2[snp]['lactose'] = [0, 0, 0, 0]


for time, rs1, rs2, rs3, rs4, rs5, earwax, eyes, asparagus, bitter, lactose in data:
    
    for i, rs in enumerate([rs1, rs2, rs3, rs4, rs5]):
        snp = snp_name[i]

        if rs == genos[i][0]:
            if lactose == 'lactose_yes':
                geno1[snp]['lactose'][0] += 1
            else:
                geno1[snp]['lactose'][1] += 1
            
            if bitter == 'bitter_yes':
                geno1[snp]['bitter'][0] += 1
            else:
                geno1[snp]['bitter'][1] += 1
            
            if asparagus == 'asparagus_yes':
                geno1[snp]['asparagus'][0] += 1
            else:
                geno1[snp]['asparagus'][1] += 1
            
            if eyes == 'brown' or eyes == 'other':
                geno1[snp]['eyes'][0] += 1
            else:
                geno1[snp]['eyes'][1] += 1
            
            if earwax == 'earwax_wet':
                geno1[snp]['earwax'][0] += 1
            else:
                geno1[snp]['earwax'][1] += 1
        
        elif rs == genos[i][1] or rs == genos[i][2] or rs == genos[i][3]:
            if lactose == 'lactose_yes':
                geno1[snp]['lactose'][2] += 1
            else:
                geno1[snp]['lactose'][3] += 1
            
            if bitter == 'bitter_yes':
                geno1[snp]['bitter'][2] += 1
            else:
                geno1[snp]['bitter'][3] += 1
            
            if asparagus == 'asparagus_yes':
                geno1[snp]['asparagus'][2] += 1
            else:
                geno1[snp]['asparagus'][3] += 1
            
            if eyes == 'brown' or eyes == 'other':
                geno1[snp]['eyes'][2] += 1
            else:
                geno1[snp]['eyes'][3] += 1
            
            if earwax == 'earwax_wet':
                geno1[snp]['earwax'][2] += 1
            else:
                geno1[snp]['earwax'][3] += 1

        if rs == genos[i][3]:
            if lactose == 'lactose_yes':
                geno2[snp]['lactose'][0] += 1
            else:
                geno2[snp]['lactose'][1] += 1
            
            if bitter == 'bitter_yes':
                geno2[snp]['bitter'][0] += 1
            else:
                geno2[snp]['bitter'][1] += 1
            
            if asparagus == 'asparagus_yes':
                geno2[snp]['asparagus'][0] += 1
            else:
                geno2[snp]['asparagus'][1] += 1
            
            if eyes == 'brown' or eyes == 'other':
                geno2[snp]['eyes'][0] += 1
            else:
                geno2[snp]['eyes'][1] += 1
            
            if earwax == 'earwax_wet':
                geno2[snp]['earwax'][0] += 1
            else:
                geno2[snp]['earwax'][1] += 1
        
        elif rs == genos[i][0] or rs == genos[i][1] or rs == genos[i][2]:
            if lactose == 'lactose_yes':
                geno2[snp]['lactose'][2] += 1
            else:
                geno2[snp]['lactose'][3] += 1
            
            if bitter == 'bitter_yes':
                geno2[snp]['bitter'][2] += 1
            else:
                geno2[snp]['bitter'][3] += 1
            
            if asparagus == 'asparagus_yes':
                geno2[snp]['asparagus'][2] += 1
            else:
                geno2[snp]['asparagus'][3] += 1
            
            if eyes == 'brown' or eyes == 'other':
                geno2[snp]['eyes'][2] += 1
            else:
                geno2[snp]['eyes'][3] += 1
            
            if earwax == 'earwax_wet':
                geno2[snp]['earwax'][2] += 1
            else:
                geno2[snp]['earwax'][3] += 1

      
        for allele in rs:
            
            if allele.find(snps[i][0]) != -1:
                if lactose == 'lactose_yes':
                    gwas[snp]['lactose'][0] += 1
                else:
                    gwas[snp]['lactose'][1] += 1
            
                if bitter == 'bitter_yes':
                    gwas[snp]['bitter'][0] += 1
                else:
                    gwas[snp]['bitter'][1] += 1
            
                if asparagus == 'asparagus_yes':
                    gwas[snp]['asparagus'][0] += 1
                else:
                    gwas[snp]['asparagus'][1] += 1
            
                if eyes == 'brown' or eyes == 'other':
                    gwas[snp]['eyes'][0] += 1
                else:
                    gwas[snp]['eyes'][1] += 1
            
                if earwax == 'earwax_wet':
                    gwas[snp]['earwax'][0] += 1
                else:
                    gwas[snp]['earwax'][1] += 1
        
            if allele.find(snps[i][1]) != -1:
                if lactose == 'lactose_yes':
                    gwas[snp]['lactose'][2] += 1
                else:
                    gwas[snp]['lactose'][3] += 1
            
                if bitter == 'bitter_yes':
                    gwas[snp]['bitter'][2] += 1
                else:
                    gwas[snp]['bitter'][3] += 1
            
                if asparagus == 'asparagus_yes':
                    gwas[snp]['asparagus'][2] += 1
                else:
                    gwas[snp]['asparagus'][3] += 1
            
                if eyes == 'brown' or eyes == 'other':
                    gwas[snp]['eyes'][2] += 1
                else:
                    gwas[snp]['eyes'][3] += 1
            
                if earwax == 'earwax_wet':
                    gwas[snp]['earwax'][2] += 1
                else:
                    gwas[snp]['earwax'][3] += 1

AlleleNums = open('/Users/gokulr/Desktop/afs-class-gene210/WWW/files/writeups/2013/Class_GWAS_Data.txt','w')
RecNums = open('/Users/gokulr/Desktop/afs-class-gene210/WWW/files/writeups/2013/Class_GWAS_Data_Rec.txt','w')
GWASANS = open('/Users/gokulr/Desktop/afs-class-gene210/WWW/files/writeups/2013/Class_GWAS_Answers.txt','w')
GWASANSREC = open('/Users/gokulr/Desktop/afs-class-gene210/WWW/files/writeups/2013/Class_GWAS_Answers_Rec.txt','w')
ODDS = open('/Users/gokulr/Desktop/afs-class-gene210/WWW/files/writeups/2013/Class_GWAS_OddsLike.txt','w')

print >> GWASANS, "phenotype",
for i,snp in enumerate(snp_name):
	print >> GWASANS, "\t" + snp,
	print >> GWASANSREC, "\t" + snp + "(" + genos[i][0] + " rec.)" + "\t" + "(" + genos[i][3] + " rec.)",
print >> GWASANS, "\n",    
print >> GWASANSREC, "\n",   
for j,phe in enumerate(phenotypes):
    print >> GWASANS, phe,
    print >> GWASANSREC, phe,
    for i,snp in enumerate(snp_name):
        print >> AlleleNums, "%s (%s):" % (phe, snp)
        print >> AlleleNums, "      %s       %s" % traits[j]
        print >> AlleleNums, "%s      %d        %d" % (snps[i][0], gwas[snp][phe][0], gwas[snp][phe][1])
        print >> AlleleNums, "%s      %d        %d" % (snps[i][1], gwas[snp][phe][2], gwas[snp][phe][3])
	fisher = "%d,%d,%d,%d" % (gwas[snp][phe][0],gwas[snp][phe][1],gwas[snp][phe][2],gwas[snp][phe][3])
        print >> RecNums, "%s (%s):" % (phe, snp)
        print >> RecNums, "      %s       %s" % traits[j]
        print >> RecNums, "%s      %d        %d" % (genos[i][0], geno1[snp][phe][0], geno1[snp][phe][1])
        print >> RecNums, "%s/%s   %d        %d\n" % (genos[i][1], genos[i][3], geno1[snp][phe][2], geno1[snp][phe][3])
        print >> RecNums, "%s      %d        %d" % (genos[i][3], geno2[snp][phe][0], geno2[snp][phe][1])
        print >> RecNums, "%s/%s   %d        %d\n" % (genos[i][0], genos[i][1], geno2[snp][phe][2], geno2[snp][phe][3])
	fisher = "%d,%d,%d,%d" % (gwas[snp][phe][0],gwas[snp][phe][1],gwas[snp][phe][2],gwas[snp][phe][3])
	rinput = "R -q -e \"chisq.test(matrix(c(%s),nrow=2),correct=FALSE)\" | grep p-value | awk \'{print $9}\'" % (fisher)
	pval = os.popen(rinput).read().rstrip() 
	print >> GWASANS, "\t" + str(pval),
	fisher = "%d,%d,%d,%d" % (geno1[snp][phe][0],geno1[snp][phe][1],geno1[snp][phe][2],geno1[snp][phe][3])
	rinput = "R -q -e \"fisher.test(matrix(c(%s),nrow=2))\" | grep p-value | awk \'{print $3}\'" % (fisher)
	pval = os.popen(rinput).read().rstrip() 
	print >> GWASANSREC, "\t" + str(pval),
	fisher = "%d,%d,%d,%d" % (geno2[snp][phe][0],geno2[snp][phe][1],geno2[snp][phe][2],geno2[snp][phe][3])
	rinput = "R -q -e \"fisher.test(matrix(c(%s),nrow=2))\" | grep p-value | awk \'{print $3}\'" % (fisher)
	pval = os.popen(rinput).read().rstrip() 
	print >> GWASANSREC, "\t" + str(pval),
    print >> GWASANS, "\n",
    print >> GWASANSREC, "\n",
    print >> AlleleNums, "\n-----------------------\n"
    print >> RecNums, "\n-----------------------\n"

lacodds = 1/((gwas['4988235']['lactose'][0]*gwas['4988235']['lactose'][3])/(gwas['4988235']['lactose'][1]*gwas['4988235']['lactose'][2]))
#eyeodds = 1/((gwas['7495174']['eyes'][0]*gwas['7495174']['eyes'][3])/(gwas['7495174']['eyes'][1]*gwas['7495174']['eyes'][2]))
eyeodds = 10000000000000
bitterodds = 1/((gwas['713598']['bitter'][0]*gwas['713598']['bitter'][3])/(gwas['713598']['bitter'][1]*gwas['713598']['bitter'][2]))
earodds = (gwas['17822931']['earwax'][0]*gwas['17822931']['earwax'][3])/(gwas['17822931']['earwax'][1]*gwas['17822931']['earwax'][2])
aspodds = (gwas['4481887']['asparagus'][0]*gwas['4481887']['asparagus'][3])/(gwas['4481887']['asparagus'][1]*gwas['4481887']['asparagus'][2])

lacLR_G = ((gwas['4988235']['lactose'][2]) / (gwas['4988235']['lactose'][0]+gwas['4988235']['lactose'][2]))  /  ((gwas['4988235']['lactose'][3]) / (gwas['4988235']['lactose'][1]+gwas['4988235']['lactose'][3]))
eyeLR_A = ((gwas['7495174']['eyes'][1]) / (gwas['7495174']['eyes'][1]+gwas['7495174']['eyes'][3])) / ((gwas['7495174']['eyes'][0]) / (gwas['7495174']['eyes'][0]+gwas['7495174']['eyes'][2])) 
bitterLR_C = ((gwas['713598']['bitter'][1]) / (gwas['713598']['bitter'][1]+gwas['713598']['bitter'][3])) / ((gwas['713598']['bitter'][0]) / (gwas['713598']['bitter'][0]+gwas['713598']['bitter'][2]))
earLR_T = ((gwas['17822931']['earwax'][3]) / (gwas['17822931']['earwax'][1]+gwas['17822931']['earwax'][3])) / ((gwas['17822931']['earwax'][2]) / (gwas['17822931']['earwax'][0]+gwas['17822931']['earwax'][2]))
aspLR_G = ((gwas['4481887']['asparagus'][3]) / (gwas['4481887']['asparagus'][1]+gwas['4481887']['asparagus'][3])) / ((gwas['4481887']['asparagus'][2]) / (gwas['4481887']['asparagus'][0]+gwas['4481887']['asparagus'][2]))

lacIR = (geno2['4988235']['lactose'][0] / (geno2['4988235']['lactose'][0]+geno2['4988235']['lactose'][1])) / ((geno2['4988235']['lactose'][0]+geno2['4988235']['lactose'][2]) / (geno2['4988235']['lactose'][0]+geno2['4988235']['lactose'][1]+geno2['4988235']['lactose'][2]+geno2['4988235']['lactose'][3]))
eyeIR = (geno1['7495174']['eyes'][1] / (geno1['7495174']['eyes'][1]+geno1['7495174']['eyes'][0])) / ((geno1['7495174']['eyes'][1]+geno1['7495174']['eyes'][3]) / (geno1['7495174']['eyes'][1]+geno1['7495174']['eyes'][0]+geno1['7495174']['eyes'][2]+geno1['7495174']['eyes'][3]))
bitIR = (geno1['713598']['bitter'][1] / (geno1['713598']['bitter'][0]+geno1['713598']['bitter'][1])) / ((geno1['713598']['bitter'][1]+geno1['713598']['bitter'][3]) / (geno1['713598']['bitter'][0]+geno1['713598']['bitter'][1]+geno1['713598']['bitter'][2]+geno1['713598']['bitter'][3]))
earIR = (geno2['17822931']['earwax'][1] / (geno2['17822931']['earwax'][1]+geno2['17822931']['earwax'][0])) / ((geno2['17822931']['earwax'][1]+geno2['17822931']['earwax'][3]) / (geno2['17822931']['earwax'][0]+geno2['17822931']['earwax'][1]+geno2['17822931']['earwax'][2]+geno2['17822931']['earwax'][3]))
aspIR = (geno2['4481887']['asparagus'][1] / (geno2['4481887']['asparagus'][1]+geno2['4481887']['asparagus'][0])) / ((geno2['4481887']['asparagus'][1]+geno2['4481887']['asparagus'][3]) / (geno2['4481887']['asparagus'][0]+geno2['4481887']['asparagus'][1]+geno2['4481887']['asparagus'][2]+geno2['4481887']['asparagus'][3]))

print >> ODDS, "Allelic Odds Ratios:"
print >> ODDS, "Lactose Intolerance, rs4988235 = %.3f" % (lacodds)
print >> ODDS, "Eye Color, rs7495174 = %.3f" % (eyeodds)
print >> ODDS, "Bitter Taste, rs713598 = %.3f" % (bitterodds)
print >> ODDS, "Earwax, rs17822931 = %.3f" % (earodds)
print >> ODDS, "Asparagus Anosmia, rs4481887 = %.3f" % (aspodds)
print >> ODDS, "\n"
print >> ODDS, "Likelihood Ratios:"
print >> ODDS, "Likelihood of Lactose Intolerance, given G allele at rs4988235 = %.3f" % (lacLR_G)
print >> ODDS, "Likelihood of Blue or Green Eyes, given A at rs7495174 = %.3f" % (eyeLR_A)
print >> ODDS, "Likelihood of inability to taste bitter, given C at rs713598 = %.3f" % (bitterLR_C)
print >> ODDS, "Likelihood of Dry Earwax, given T at rs17822931 = %.3f" % (earLR_T)
print >> ODDS, "Likelihood of Asparagus Anosmia, given G at rs4481887 = %.3f" % (aspLR_G)

print >> ODDS, "\n"
print >> ODDS, "Relative Risk:"
print >> ODDS, "Lactose Intolerance with GG genotype at rs4988235 = %.3f" % (lacIR)
print >> ODDS, "Blue/Green Eye Color with AA genotype at rs7495174 = %.3f" % (eyeIR)
print >> ODDS, "Inability to taste bitter with CC genotype at rs713598 = %.3f" % (bitIR)
print >> ODDS, "Dry Earwax with TT genotype at rs17822931 = %.3f" % (earIR)
print >> ODDS, "Asparagus Anosmia with GG genotype at rs4481887 = %.3f" % (aspIR)
