use warnings;
use strict;

open (my $INPUT, "<", "als_snps.txt");

open (my $OUTPUT, ">", "als_addmysql.txt");

while (<$INPUT>) {
	chomp;
	my @fields = split(/\t/,$_);
	my ($pheno, $gene, $dbsnp, $major, $minor, $risk, $OR, $pop) = @fields;
	print $OUTPUT "insert into neuro\_ALS \(dbsnp\,gene\,major\,minor\,risk\,oddsratio\,pop) values \(\'$dbsnp\'\,\'$gene\'\,\'$major\'\,\'$minor\'\,\'$risk\'\,\'$OR\'\,\'$pop\'\)\;\n";
}
close $INPUT;
close $OUTPUT;
