scores <- read.table("INTELLIGENCE_RESULTS.txt", sep="\t", header=TRUE)

pdf("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/intelligence.pdf")
attach(scores)
hist(scoress,xlim=c(0,1), breaks = c(0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1), xlab="Intelligence Allele Fraction", ylab="Number of Individuals", main = "Intelligence Alleles")
dev.off()

