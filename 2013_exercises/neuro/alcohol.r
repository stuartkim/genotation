
source('/Users/gokulr/Gokul/Classes/personalgenomics/TA2013/exercises/get-connection.r')

selection <- dbGetQuery(conn, "SELECT * FROM unified where exercise = 'neuro_alcohol' and `key` = 671;")
attach(selection)
counts <- table(value)
png("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/alcohol_671.png") 
barplot(counts, main = "rs671", xlab = "Genotype", ylab = "Number of individuals")
dev.off()

selection <- dbGetQuery(conn, "SELECT * FROM unified where exercise = 'neuro_alcohol' and `key` = 1614792;")
attach(selection)
counts <- table(value)
png("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/alcohol_1614792.png") 
barplot(counts, main = "rs1614792", xlab = "Genotype", ylab = "Number of individuals")
dev.off()

selection <- dbGetQuery(conn, "SELECT * FROM unified where exercise = 'neuro_alcohol' and `key` = 7590720;")
attach(selection)
counts <- table(value)
png("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/alcohol_7590720.png") 
barplot(counts, main = "rs7590720", xlab = "Genotype", ylab = "Number of individuals")
dev.off()
