use warnings;
use strict;

open (my $INPUT, "<", "intelligence_snps.txt");

open (my $OUTPUT, ">", "intelligence_addmysql.txt");

while (<$INPUT>) {
	chomp;
	my @fields = split(/\t/,$_);
	my ($dbsnp, $gene, $major, $minor, $risk, $freq, $pval, $OR, $pop) = @fields;
	print $OUTPUT "insert into neuro\_intelligence \(dbsnp\,gene\,major\,minor\,risk\,risk_freq\,pvalue\,oddsratio) values \(\'$dbsnp\'\,\'$gene\'\,\'$major\'\,\'$minor\'\,\'$risk\'\,\'$freq\'\,\'$pval\'\,\'$OR\'\)\;\n";
}
close $INPUT;
close $OUTPUT;
