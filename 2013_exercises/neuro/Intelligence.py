"""

"""
from __future__ import division
import math
import sys
import os
import MySQLdb



db = MySQLdb.connect(host="fulltext.stanford.edu", port=3306, user="tomadmin", passwd="ooze4PhD", db="interpretome_exercises")
c = db.cursor()

query = """
select *
from unified where exercise = 'neuro_intelligence';
"""

c.execute(query)

data = c.fetchall()

SNPLIST = open('intelligence_snps.txt','r')

snplist = dict()

for line in SNPLIST:
	fields = line.rstrip().split("\t")
	snp = fields[0]
	risk = fields[4]
	snplist[snp] = risk  


alleles = dict()
total = dict()
for ID, time, snp, genotype, population, person, exercise in data:
	person = str(person)
	snp2 = 'rs' + str(snp)
	if person in alleles:
		alleles[person] = alleles[person] + genotype.count(snplist[snp2])
		if genotype != '??':
			total[person] = total[person] + 2;
	else:
		alleles[person] = genotype.count(snplist[snp2])
		if genotype != '??':
			total[person] = 2;	


scores = open('INTELLIGENCE_RESULTS.txt','w')

print >> scores, 'scoress'

for person in alleles:
	fract = float(alleles[person]) / float(total[person])
	print >> scores, fract

	
