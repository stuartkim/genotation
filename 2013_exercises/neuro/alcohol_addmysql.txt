insert into neuro_alcohol (dbsnp,gene,major,minor,risk,risk_freq,pvalue,oddsratio,pop) values ('rs671','ALDH2','G','A','A','0.162','3.00E-56','4.35','CHB');
insert into neuro_alcohol (dbsnp,gene,major,minor,risk,risk_freq,pvalue,oddsratio,pop) values ('rs671','ALDH2','G','A','A','0.162','3.00E-56','4.35','JPT');
insert into neuro_alcohol (dbsnp,gene,major,minor,risk,risk_freq,pvalue,oddsratio,pop) values ('rs1614792','ADH1C','T','C','T','0.763','0.0017','1.25','JPT');
insert into neuro_alcohol (dbsnp,gene,major,minor,risk,risk_freq,pvalue,oddsratio,pop) values ('rs1614792','ADH1C','T','C','T','0.763','0.0017','1.25','CHB');
insert into neuro_alcohol (dbsnp,gene,major,minor,risk,risk_freq,pvalue,oddsratio,pop) values ('rs1614792','ADH1C','T','C','T','0.763','0.0017','1.25','CEU');
insert into neuro_alcohol (dbsnp,gene,major,minor,risk,risk_freq,pvalue,oddsratio,pop) values ('rs7590720','PECR','A','G','G','0.281','9.72E-09','1.35','CEU');
