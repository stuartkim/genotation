source('/Users/gokulr/Gokul/Classes/personalgenomics/TA2013/exercises/get-connection.r')

#gwas <- dbGetQuery(conn, "SELECT * FROM class_gwas where`4988235` is not null AND submit_time < '2013-01-01';")
height <- dbGetQuery(conn, "SELECT * FROM class_height where actual between 147 and 215;")
attach(height)

regr = lm(family~actual)
png("Family_vs_Actual_AllData.png")
plot(actual,family, pch=21, bg="black",xlim = c(150,200), ylim = c(150,200), xlab= "Actual height (cm)", ylab = "Family height (cm)", main = paste("Actual vs predicted family height, correlation=", format(summary(regr)$adj.r.squared,digits=3)))
abline(regr, col="red")
dev.off() 

regr2 = lm(genetic~actual)
png("Genetic_vs_Actual_AllData.png")
plot(actual,genetic, pch=21, bg="black",xlim = c(150,200), ylim = c(150,200), xlab= "Actual height (cm)", ylab = "Genetic height (cm)", main = paste("Actual vs predicted genetic height, correlation=", format(summary(regr2)$adj.r.squared,digits=3)))
abline(regr2, col="red")
dev.off() 

detach(height)

height_thisyear <- dbGetQuery(conn, "SELECT * FROM class_height where submit_time > '2013-01-01' and actual between 147 and 215;")
attach(height_thisyear)

regr3 = lm(family~actual)
png("Family_vs_Actual_2013.png")
plot(actual,family, pch=21, bg="black",xlim = c(150,200), ylim = c(150,200), xlab= "Actual height (cm)", ylab = "Family height (cm)", main = paste("Actual vs predicted family height, correlation=", format(summary(regr3)$adj.r.squared,digits=3)))
abline(regr, col="red")
dev.off() 

regr4 = lm(genetic~actual)
png("Genetic_vs_Actual_2013.png")
plot(actual,genetic, pch=21, bg="black",xlim = c(150,200), ylim = c(150,200), xlab= "Actual height (cm)", ylab = "Genetic height (cm)", main = paste("Actual vs predicted genetic height, correlation=", format(summary(regr4)$adj.r.squared,digits=3)))
abline(regr4, col="red")
dev.off() 
