use warnings;
use strict;

open (my $INPUT, "<", "pgx_snps.txt");

open (my $OUTPUT, ">", "pgx_addmysql.txt");

while (<$INPUT>) {
	chomp;
	my @fields = split(/\t/,$_);
	my ($dbsnp, $gene, $wildtype, $mutant, $function, $drug) = @fields;
	print $OUTPUT "insert into pgx\_2013 \(dbsnp\,gene\,wildtype\,mutant\,function\,drug) values \(\'$dbsnp\'\,\'$gene\'\,\'$wildtype\'\,\'$mutant\'\,\'$function\'\,\'$drug\'\)\;\n";
}
close $INPUT;
close $OUTPUT;
