
source('/Users/gokulr/Gokul/Classes/personalgenomics/TA2013/exercises/get-connection.r')

selection <- dbGetQuery(conn, "SELECT * FROM unified where exercise = 'pgx_2013' and `key` = 3892097;")
attach(selection)
counts <- table(value)
png("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/pgx_3892097.png") 
barplot(counts, main = "rs3892097", xlab = "Genotype", ylab = "Number of individuals")
dev.off()

selection <- dbGetQuery(conn, "SELECT * FROM unified where exercise = 'pgx_2013' and `key` = 5030655;")
attach(selection)
counts <- table(value)
png("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/pgx_5030655.png") 
barplot(counts, main = "rs5030655", xlab = "Genotype", ylab = "Number of individuals")
dev.off()

selection <- dbGetQuery(conn, "SELECT * FROM unified where exercise = 'pgx_2013' and `key` = 4149056;")
attach(selection)
counts <- table(value)
png("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/pgx_4149056.png") 
barplot(counts, main = "rs4149056", xlab = "Genotype", ylab = "Number of individuals")
dev.off()
