
source('/Users/gokulr/Gokul/Classes/personalgenomics/TA2013/exercises/get-connection.r')

selection <- dbGetQuery(conn, "SELECT * FROM class_warfarin where submit_time > '2013-05-07';")
attach(selection)

png("/Users/gokulr/Desktop/afs-class-gene210/WWW/files/exercises/2013/warfarin_scatterplot.png")
plot(extended,clinical, pch=21,cex.main=1, bg="black", xlim=c(20,80),ylim=c(20,80), xlab= "Extended Dose", ylab = "Clinical Dose", main = "Warfarin Dosing")
dev.off()
