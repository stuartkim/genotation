use warnings;
use strict;

open (my $INPUT, "<", "melanoma_snps.txt");

open (my $OUTPUT, ">", "melanoma_addmysql.txt");

while (<$INPUT>) {
	chomp;
	my @fields = split(/\t/,$_);
	my ($dbsnp, $gene, $major, $minor, $risk, $OR, $pval) = @fields;
	print $OUTPUT "insert into gitler\_cancer\_melanoma \(rsid\,gene\,major\,minor\,risk\,oddsratio\,pvalue) values \(\'$dbsnp\'\,\'$gene\'\,\'$major\'\,\'$minor\'\,\'$risk\'\,\'$OR\'\,\'$pval\'\)\;\n";
}
close $INPUT;
close $OUTPUT;
