/* Do this for each Exercise:SNP pair (9 total) */
SELECT exercise, value, count(value) 
	FROM unified WHERE `key`=12426597 AND exercise="kim_linkage_p1" 
	GROUP BY value;
	
/* or more generalized for all SNPs in an exercise */
SELECT exercise, `key`, value AS "Genotype", count(value) AS "Freq"
	FROM unified
	WHERE exercise="kim_linkage_p3"
	GROUP BY `key`, value
	ORDER BY exercise, `key`, value;
	
	
	
	
SELECT exercise, sid, GROUP_CONCAT(value) AS "Genotype", count(value) AS "Freq"
	FROM unified
	WHERE exercise="kim_linkage_p3"
	GROUP BY sid, value;
	
	

/* HERE IT IS !!!!!!! --- HAPLOTYPES  */
SELECT Haplotype, count(Haplotype) FROM (SELECT sid, GROUP_CONCAT(value ORDER BY `key`) AS "Haplotype" FROM unified WHERE exercise="kim_linkage_p3" GROUP BY sid) AS T1 GROUP BY Haplotype;



/* Exercise 1 Queries */
SELECT exercise, value, count(value) 
	FROM unified WHERE `key`=6447271 AND exercise="kim_linkage_p1" 
	GROUP BY value;
	
SELECT exercise, value, count(value) 
	FROM unified WHERE `key`=12426597 AND exercise="kim_linkage_p1" 
	GROUP BY value;
	
SELECT exercise, value, count(value) 
	FROM unified WHERE `key`=878198 AND exercise="kim_linkage_p1" 
	GROUP BY value;
	