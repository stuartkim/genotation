import random
import numpy
from namedmatrix import NamedMatrix


def get_maxes(scores):
    
    maxes = list()
    for i, score in enumerate(scores):
        if score == max(scores):
            maxes.append(i)
    return maxes

P = 9
N = 14

x = numpy.random.binomial(1, 0.25, (N,9))

phenotypes = ['earwax', 'eye color', 'warfarin', 'cf', 'height', 'apoe', 'td2m', 'weight', 'statin', 'clopidogrel', 'hair color', 'lactose', 'bitter', 'sickle cell']
data = NamedMatrix(x, phenotypes)

y = [0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0]


scores = [0]*P

order = range(data.size(0))
random.shuffle(order)

for j in order: # phenotypes
    for i in range(data.size(1)): # people
        if data[j,i] == y[j]:
            scores[i] += 1
    
    print j, scores, get_maxes(scores)

#
Non Caucasian = 0, 2
Female = 7
Culprit = 5

NamedMatrix:
                   0          1          2          3          4          5          6          7          8 
    earwax  0.000e+00, 0.000e+00, 1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00,
 eye color  0.000e+00, 1.000e+00, 1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 1.000e+00, 0.000e+00,
  warfarin  0.000e+00, 1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00,
        cf  0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00,
    height  1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 1.000e+00, 0.000e+00, 1.000e+00, 0.000e+00, 0.000e+00,
      apoe  1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 1.000e+00, 0.000e+00,
      td2m  1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 1.000e+00,
    weight  0.000e+00, 1.000e+00, 1.000e+00, 1.000e+00, 0.000e+00, 1.000e+00, 0.000e+00, 1.000e+00, 0.000e+00,
    statin  0.000e+00, 1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00,
clopidogrel  0.000e+00, 1.000e+00, 0.000e+00, 1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00,
hair color  0.000e+00, 0.000e+00, 0.000e+00, 1.000e+00, 1.000e+00, 1.000e+00, 1.000e+00, 1.000e+00, 0.000e+00,
   lactose  0.000e+00, 0.000e+00, 1.000e+00, 0.000e+00, 0.000e+00, 1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00,
    bitter  0.000e+00, 0.000e+00, 0.000e+00, 1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00,
sickle cell  0.000e+00, 1.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 0.000e+00, 1.000e+00, 1.000e+00,