source('../get-connection.r')

library(ggplot2)
library(stringr)

query <- '
	SELECT * FROM exercises.unified_2012 u
	JOIN exercises.mignot_narcolepsy m ON (u.`key` = m.dbsnp)
	WHERE exercise = "mignot_narcolepsy" AND value != "??";
'

query <- '
	SELECT * FROM exercises.unified_2012 u
	WHERE exercise = "mignot_narcolepsy" AND `key` = "submit" AND id > 1428;
'

dat <- dbGetQuery(conn, query)
dat$value <- as.numeric(dat$value)
ggplot(dat, aes(value)) + 
	geom_histogram() +
	scale_x_continuous('Cumulative odds ratio') +
	scale_y_continuous('Number of people')

ggsave('aggregated.png')

stop()

dat$nr <- vapply(seq_len(nrow(dat)), function(i) {
	sum(unlist(strsplit(dat$value[i], '', fixed = TRUE)) == dat$risk[i])
}, integer(1))

dat$value <- vapply(seq_len(nrow(dat)), function(i) {
	res <- str_c(
		sort(unlist(strsplit(dat$value[i], '', fixed = TRUE))), collapse = ''
	)
	if (res == 'GA') browser()
	res
}, character(1))

d_ply(dat, .(dbsnp), function(d) {
	d$value <- factor(d$value)
	if ('AA' %in% levels(d$value))
		d$value <- relevel(d$value, 'AA')

	ggplot(d, aes(value, fill = value)) +
		geom_histogram() +
		scale_x_discrete('Genotype') +
		scale_y_continuous('Number of people') +
		scale_fill_discrete(NULL) +
		opts(
			title = sprintf('Gene: %s, risk allele: %s', d$gene[1], d$risk[1]),
			legend.position = 'none'
		)

		ggsave(str_c(d$dbsnp[1], '.png'))
})