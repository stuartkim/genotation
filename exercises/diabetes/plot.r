source('../get-connection.r')

library(ggplot2)

data <- dbGetQuery(conn, '
	SELECT * FROM exercises.unified 
	JOIN exercises.diabetes ON (diabetes.dbsnp = unified.key)
	WHERE exercise = "class_diabetes" AND value != "??" AND unified.id > 408;'
)

d_ply(data, .(key), function(d) {
	ggplot(d, aes(value, fill = value)) +	
		geom_histogram() + 
		opts(title = sprintf(
			'dbSNP: %s; genes: %s; risk allele: %s; OR: %0.2f', 
			d$key[1], d$genes[1], d$risk[1], d$or[1]
		)) +
		scale_x_discrete('Genotype') +
		scale_y_continuous('Number of individuals')
	ggsave(sprintf('t2dm-count-%s.png', d$key[1]))
})

