library(RMySQL)

if (!exists('conn')) {
  conn <- dbConnect(
    'MySQL', host = 'esquilax', 
    user = 'gene210-admin', pass = 'GWAS4life'
  )
}

data <- dbGetQuery(conn, 'SELECT * FROM exercises.class_cad;')
names(data)[-(1:2)] <- paste('rs', names(data)[-c(1:2)], sep = '')

data[data == 'NA'] <- NA
data <- data[, colSums(is.na(data)) != nrow(data)]

genotypes <- apply(data[, -c(1, 2)], 2, function(genotypes) {
  table(genotypes)
})
alleles <- apply(data[, -c(1, 2)], 2, function(alleles) {
  table(unlist(strsplit(alleles, '', fixed = TRUE)))
})

risk.alleles <- dbGetQuery(conn, 'SELECT * FROM exercises.cad;')
snps <- intersect(risk.alleles$dbsnp, names(alleles))
class.results <- do.call(rbind, lapply(snps, function(s) {
  if (length(names(alleles[[s]])) == 1) 
    return(NULL)
  
  risk.allele <- risk.alleles$risk_allele[risk.alleles$dbsnp == s]
  risk.frequency <- risk.alleles$risk_frequency[risk.alleles$dbsnp == s]
  which.risk <- which(names(alleles[[s]]) == risk.allele)

  result <- data.frame(
    snp = s, risk = risk.allele, risk.count = alleles[[s]][which.risk],
    nonrisk.count = alleles[[s]][-which.risk],
    risk.frequency = risk.frequency
  )
}))
class.results$class.frequency <- class.results$risk.count / 
  (class.results$risk.count + class.results$nonrisk.count)

write.csv(
  class.results, '~/Dropbox/Gene210/exercises/ashley-cad/cad-results.csv', 
  row.names = FALSE
)
