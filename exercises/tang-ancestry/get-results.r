library(ggplot2)

source('../get-connection.r')

ancestry.data <- dbGetQuery(conn, 'SELECT * FROM exercises.ancestry;')
class.data <- dbGetQuery(conn, 'SELECT * FROM exercises.class_ancestry;')
rsids <- names(class.data)[-(1:3)]

ceu.populations <- c('TSI')
for (population in ceu.populations) {
  class.data$population[class.data$population == population] <- 'CEU'
}

sapply(rsids, function(rsid) {
  m <- ancestry.data$dbsnp == paste('rs', rsid, sep = '')
  euro.allele <- ancestry.data$euro_allele[m]

  count_allele <- function(alleles) {
    sum(alleles == euro.allele)
  }

  class.data[[rsid]] <<- vapply(
    strsplit(class.data[[rsid]], '', fixed = TRUE), count_allele, integer(1)
  )
})


write.csv(class.data[, -c(1, 3)], 'class-ancestry.csv', row.names = FALSE)

sapply(rsids, function(rsid) {
  cat('\n', rsid, '\n')
  print(table(class.data$population, class.data[[rsid]]))
})

counts <- data.frame(
  population = class.data$population,
  count = rowSums(class.data[, -(1:3)])
)

class.data <- melt(
  class.data, id.vars = c('id', 'population', 'submit_time'), 
  variable_name = 'rsid'
)
ggplot(class.data, aes(x = value, color = population)) + 
  opts(
    title = "Respondents with 0, 1, and 2 'European' alleles"
  ) + 
  scale_x_continuous('', breaks = NA) +
  scale_y_continuous('Number of respondents') +
  geom_histogram() + 
  facet_wrap(~ rsid)
ggsave('ancestry-snps.png')

ggplot(counts, aes(x = count)) +
  opts(title = 'Distribution of allele counts') +
  scale_x_continuous('Number of European alleles') + 
  scale_y_continuous('Proportion of class') + 
  geom_density() + 
  geom_rug(aes(x = count, color = population), size = 0.75)
ggsave('ancestry-counts.png')
