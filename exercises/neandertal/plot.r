library(ggplot2)

source('../get-connection.r')

dat <- dbGetQuery(conn, '
	SELECT value, population FROM exercises.unified
	WHERE exercise = "class_neandertal";'
)

dat$value <- as.integer(dat$value)

ggplot(dat, aes(value)) + geom_histogram() +
opts(title = 'Are you a neandertal?') +
scale_y_continuous('Number of people') +
scale_x_continuous('Number of neandertal alleles')
ggsave('neandertal.png')
