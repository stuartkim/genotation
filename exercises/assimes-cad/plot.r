source('../get-connection.r')

library(ggplot2)

dat <- dbGetQuery(conn, '
	SELECT * FROM interpretome_exercises.unified_2012
	WHERE exercise = "class_assimes" AND
	`key` IN ("Total", "Number of risk alleles");'
)
plot.dat <- data.frame(value = 
	as.integer(subset(dat, key == 'Number of risk alleles')$value) /
	as.integer(subset(dat, key == 'Total')$value)
)

ggplot(plot.dat, aes(value)) + 
geom_histogram() +
scale_x_continuous('Number of risk alleles') +
scale_y_continuous('Number of people') +
opts(title = 'Risk distribution for class')
ggsave('assimes-cad-all_TEST.png')



