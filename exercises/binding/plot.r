source('../get-connection.r')

library(ggplot2)

dbsnps <- c('6135095', '2844792')

dat <- dbGetQuery(conn, sprintf('
	SELECT * FROM exercises.unified 
	WHERE `key` IN (%s) AND exercise = "snyder_binding" 
	AND value != "??";',
	paste(sprintf('%s', dbsnps), collapse = ', ')
))

ggplot(dat, aes(value, fill = value)) +	
	geom_histogram() + 
	facet_grid(~ key ) +
	opts(title ='Genotype distributions for binding SNPs') +
	scale_x_discrete('Genotype') +
	scale_y_continuous('Number of individuals')
ggsave('binding.png')

