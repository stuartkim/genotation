library(ggplot2)
source('../get-connection.r')

dat <- dbGetQuery(conn, '
	SELECT * FROM exercises.unified
	WHERE `key` IN ("prior", "estimate");'
)

plot.dat <- subset(dat, select = c(key, value))
plot.dat$value <- as.numeric(plot.dat$value)
plot.dat$id <- rep(1:(nrow(plot.dat) / 2), each = 2)
ggplot(plot.dat, aes(value, id, group = key, color = key)) + 
geom_point() + 
opts(title = 'Likelihood ratio risk estimation') +
scale_x_continuous('Probability') + 
scale_y_continuous('Person') +
scale_color_discrete('Estimate') +
ggsave('riskograms.png')

