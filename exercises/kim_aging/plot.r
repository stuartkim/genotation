source('../get-connection.r')

library(ggplot2)
library(stringr)

query <- '
	SELECT * FROM exercises.unified
	JOIN exercises.kim_aging ON (dbsnp = `key`)
	WHERE exercise = "kim_aging" AND value != "??";
'

dat <- dbGetQuery(conn, query)

dat$n <- vapply(seq_len(nrow(dat)), function(i) {
	alleles <- unlist(strsplit(dat$value[i], '', fixed = TRUE))
	sum(alleles == dat$longevity_allele[i])
}, integer(1))


dat$value <- vapply(seq_len(nrow(dat)), function(i) {
	str_c(sort(unlist(strsplit(dat$value[i], '', fixed = TRUE))), collapse = '')
}, character(1))

d_ply(dat, .(dbsnp), function(d) {
	d$value <- factor(d$value)
	if ('AA' %in% levels(d$value))
		d$value <- relevel(d$value, 'AA')

	ggplot(d, aes(value, fill = value)) +
		geom_histogram() +
		scale_x_discrete('Genotype') +
		scale_y_continuous('Number of people') +
		scale_fill_discrete(NULL) +
		opts(
			title = sprintf('Gene: %s, risk allele: %s', d$gene[1], d$longevity_allele[1]),
			legend.position = 'none'
		)

		ggsave(str_c(d$dbsnp[1], '.png'))
})


query <- '
	SELECT * FROM exercises.unified
	WHERE exercise = "longevity";
'
dat <- dbGetQuery(conn, query)
dat$value <- as.numeric(dat$value)
ggplot(dat, aes(value)) + 
	geom_histogram() +
	scale_y_continuous('Number of people') +
	opts(title = 'Probability of extreme longevity')
	
last_plot() + scale_x_continuous('Pr(EL) (in percent)')
ggsave('extreme_longevity.png')

breaks <- c(0, 2 ^ seq(0, 6), 100)
last_plot() + 
	scale_x_log(
		'Pr(EL) in percent', labels = breaks, breaks = breaks
	)
ggsave('extreme_longevity_log.png')
