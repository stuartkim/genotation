require 'csv'

line_match = /^(\S+) (\S+) (\S+) (\S+) (\S+)(.+)rs(\d+)/

CSV.open('processed.csv', 'w') do |csv|
	open('snps.txt').each do |line|
		match = line_match.match(line)
		csv << match.captures.map {|c| c.strip}
	end
end

populations = {
	'africa' => 'YRI',
	'asia' => 'JPT',
	'ceu' => 'CEU',
	'asia' => 'CHB'

}

CSV.open('processed-snps.csv', 'w') do |csv|
	csv << ['dbsnp', 'ancestral', 'derived', 'pop', 'genes']
	populations.each_pair do |filename, population|
		lines = open(filename + '.csv').map {|line| line.split}
		lines.each do |line| 
			line[2] = line[2] == '?' ? 'NULL' : line[2]
			line[0] = line[0][2..-1]
			line << population << 'NULL'
			csv << line
		end
	end
end
