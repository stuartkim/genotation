CREATE TABLE `selection` (
  `dbsnp` INTEGER NOT NULL,
  `ancestral` varchar(1),
  `derived` varchar(1),
  `pop` varchar(255) NOT NULL,
  `genes` varchar(255) DEFAULT NULL
);


ALTER TABLE selection ADD COLUMN id SERIAL FIRST;

UPDATE selection s, selection_copy sc 
SET s.genes = sc.genes
WHERE s.dbsnp = sc.dbsnp;

