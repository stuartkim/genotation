library(ggplot2)

source('../get-connection.r')

dat <- dbGetQuery(conn, '
	SELECT * FROM exercises.unified
	WHERE exercise = "class_selection"
	AND `key` = "Total selected:";'
)

dat$value <- as.integer(dat$value)

ggplot(dat, aes(value)) + 
geom_histogram() + facet_grid(~ population) +
scale_x_continuous('Number of selected alleles') +
scale_y_continuous('Number of people') +
opts(title = 'Selected alleles')
ggsave('selection.png')
