"""

"""

import sys
import os
import MySQLdb


db = MySQLdb.connect(host="localhost", port=3307, user="gene210-admin", passwd="GWAS4life", db="exercises")
c = db.cursor()

query = """
select *
from class_gwas
where `4988235` is not null
"""
c.execute(query)

data = c.fetchall()

snps = [('A', 'G'),
        ('A', 'G'),
        ('C', 'G'),
        ('C', 'T'),
        ('A', 'G')]

snp_name = ['4988235', '7495174', '713598', '17822931', '4481887']

phenotypes = ['earwax', 'eyes', 'asparagus', 'bitter', 'lactose']

traits = [  ('Wet', "Dry"),
            ('Brown/Other', 'Blue/Green'),
            ('Yes', 'No'),
            ('Yes', 'No'),
            ('Yes', 'No')]

gwas = dict()

for snp in snp_name:
    gwas[snp] = dict()
    gwas[snp]['earwax'] = [0, 0, 0, 0]
    gwas[snp]['eyes'] = [0, 0, 0, 0]
    gwas[snp]['asparagus'] = [0, 0, 0, 0]
    gwas[snp]['bitter'] = [0, 0, 0, 0]
    gwas[snp]['lactose'] = [0, 0, 0, 0]


for time, rs1, rs2, rs3, rs4, rs5, earwax, eyes, asparagus, bitter, lactose in data:
    
    for i, rs in enumerate([rs1, rs2, rs3, rs4, rs5]):
        snp = snp_name[i]
        
        for allele in rs:
            
            if allele.find(snps[i][0]) != -1:
                if lactose == 'lactose_yes':
                    gwas[snp]['lactose'][0] += 1
                else:
                    gwas[snp]['lactose'][1] += 1
            
                if bitter == 'bitter_yes':
                    gwas[snp]['bitter'][0] += 1
                else:
                    gwas[snp]['bitter'][1] += 1
            
                if asparagus == 'asparagus_yes':
                    gwas[snp]['asparagus'][0] += 1
                else:
                    gwas[snp]['asparagus'][1] += 1
            
                if eyes == 'brown' or eyes == 'other':
                    gwas[snp]['eyes'][0] += 1
                else:
                    gwas[snp]['eyes'][1] += 1
            
                if earwax == 'earwax_wet':
                    gwas[snp]['earwax'][0] += 1
                else:
                    gwas[snp]['earwax'][1] += 1
        
            if allele.find(snps[i][1]) != -1:
                if lactose == 'lactose_yes':
                    gwas[snp]['lactose'][2] += 1
                else:
                    gwas[snp]['lactose'][3] += 1
            
                if bitter == 'bitter_yes':
                    gwas[snp]['bitter'][2] += 1
                else:
                    gwas[snp]['bitter'][3] += 1
            
                if asparagus == 'asparagus_yes':
                    gwas[snp]['asparagus'][2] += 1
                else:
                    gwas[snp]['asparagus'][3] += 1
            
                if eyes == 'brown' or eyes == 'other':
                    gwas[snp]['eyes'][2] += 1
                else:
                    gwas[snp]['eyes'][3] += 1
            
                if earwax == 'earwax_wet':
                    gwas[snp]['earwax'][2] += 1
                else:
                    gwas[snp]['earwax'][3] += 1
        
for j,phe in enumerate(phenotypes):
    for i,snp in enumerate(snp_name):
        print >> sys.stdout, "%s (%s):" % (phe, snp)
        print >> sys.stdout, "      %s       %s" % traits[j]
        print >> sys.stdout, "%s      %d        %d" % (snps[i][0], gwas[snp][phe][0], gwas[snp][phe][1])
        print >> sys.stdout, "%s      %d        %d" % (snps[i][1], gwas[snp][phe][2], gwas[snp][phe][3])
    
    print >> sys.stdout, "\n-----------------------\n"
