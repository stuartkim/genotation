import sys
import make_frequency_tables
import make_ld_tables

populations = ('ANY', 'ASW', 'CEU', 'CHB', 'CHD', 'GIH', 'JPT', 'LWK', 'MEX', 'MKK', 'TSI', 'YRI')
populations = ('ASW', 'CHD', 'GIH', 'JPT', 'LWK', 'MKK', 'TSI')
ld_cutoff = 0.3

for population in populations:
  make_frequency_tables.main([population])
  make_ld_tables.main([population, ld_cutoff])
