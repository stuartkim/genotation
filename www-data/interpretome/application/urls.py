from django.conf.urls.defaults import *

# First argument to patterns can be a prefix.
urlpatterns = patterns('interpretome.application.views',
  (r'^$', 'index'),
  (r'^submit/$', 'submit'),
  (r'lookup/exercise/$', 'exercise'),
  (r'lookup/linked/$', 'linked'),
  (r'lookup/impute/$', 'impute'),
  (r'lookup/get_reference_alleles/$', 'get_reference_alleles'),
  (r'lookup/get_allele_frequencies/$', 'get_allele_frequencies'),
  (r'lookup/get_allele_frequencies1/$', 'get_allele_frequencies1'),
  (r'lookup/get_allele_freqs/$', 'get_allele_freqs'),
  (r'lookup/update_people/$', 'update_people'),
  (r'lookup/add_people_snps/$', 'add_people_snps'),
  (r'lookup/get_chrom_pos/$', 'get_chrom_pos'),
  
  (r'^get_snps_on_map/$', 'get_snps_on_map'),
  (r'^get_hgdp_allele_frequencies/$', 'get_hgdp_allele_frequencies'),
  
  (r'similarity/get_individuals/$', 'get_individuals'),
  (r'pca/get_pca_parameters/$', 'get_pca_parameters'),
  (r'diabetes/get_diabetes_snps/$', 'get_diabetes_snps'),

  (r'disease/get_gwas/$', 'get_gwas_disease'),
  (r'disease/impute_table/$', 'impute_table'),

  (r'disease/get_gwas_specific/$', 'get_gwas_specific'),
  (r'disease/get_gwas_multiple/$', 'get_gwas_multiple'),
  (r'disease/get_gwas_multiply/$', 'get_gwas_multiply'),
  
  (r'disease/get_gwas_catalog/$', 'get_gwas_catalog'),
  (r'height/get_height_snps/$', 'get_height_snps'),
  (r'neandertal/get_neandertal_snps/$', 'get_neandertal_snps'),
  (r'selection/get_selection_snps/$', 'get_selection_snps'),
  #(r'lookup/longevity/$', 'longevity'),
  #(r'lookup/neandertal/$', 'get_neandertal_snps'),
  
  (r'submit/$', 'submit'),
  (r'submit/submit_height_snps/$', 'submit_height_snps'),
  (r'submit/submit_neandertal_snps/$', 'submit_neandertal_snps'),
  (r'submit/submit_gwas_snps/$', 'submit_gwas_snps'),
  (r'submit/submit_doses/$', 'submit_doses'),
  #(r'submit/submit_coordinates/$', 'submit_coordinates'),
  (r'^bmd2/$', 'bmd2'),
  (r'bmd2/impute_table/$', 'impute_table'),
  (r'^diabetes/$', 'diabetes'),
  (r'^get_pharmacogenomics_snps/$', 'get_pharmacogenomics_snps'),
  
  (r'^get_painting_params/$', 'get_painting_params'),
  (r'^get_phasing_params/$', 'get_phasing_params'),
  
  (r'^get_rare_variants/$', 'get_rare_variants'),
  (r'^get_drug_targets/$', 'get_drug_targets'),
  (r'^get_polyphen_scores/$', 'get_polyphen_scores'),
  
  (r'^exercise_charts$', 'get_exercise_charts'),
)