var view_functions = {


//CLICKING BUTTON
click_button: function(event, view) {

  var self = view;

      //get the id of the button clicked. 'id' from get gwas. 
      var id = $(event.target).closest('button').attr('id');
      var obj;

      //for each item in the button list
      for (disease in self.buttons) {
        if (!self.buttons.hasOwnProperty(disease)) {
          continue;
        }

        obj = self.buttons[disease];
        //do click disease passing the object
        if (typeof self.buttons[disease] !== 'string' && obj.id === id)
        {
        //call click_disease function
          this.click_disease(event, obj, self);
          break;
        }
      }

    },


//LOADING THE PAGE
render: function(view) {
  var self = view;
  var it = this;

  $.get('/media/template/'+view.buttons.html+'.html', function(data){it.loaded(data, self)});
  $.get('/media/template/tablelegends.html', function(data){it.loaded(data, self)});

},
loaded: function(response, view) {
  $(view.el).append(response);
  $('button').button();

  if(!view.has_loaded){
    var sorted = [];
    match_style(view.el);
    view.has_loaded = true;
  }
  this.runningTotal = 0;
  this.diseaseToGet = "";
  this.disease = ""
},

  //NORMAL DISEASE LOOKUP

  got_diseases: function(response, table, view, object) {
   
    var self = this;
    user = get_user();
    self.runningTotal = 0;
    object.running_total = 0;
    
  if (object.geneticRiskScore === 'yes'){
        $(table+' tr:last').append('<th>Score</th>');
      }
    $.each(response, function(i, v) {
      
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined && dbsnp.genotype != "--"){ // if the user's genotype is defined at this snp
        v['genotype'] = dbsnp.genotype;  
        if(v['risk_allele'].charAt(0) !== "?" && !isNaN(v['or_or_beta'])) { 
          self.runningTotal += calculate_lr(v['risk_allele'], v['genotype'], v['or_or_beta']);   
        }
        $(table+' > tbody').append(_.template(view.disease_template, v));
        
        if (object.geneticRiskScore === 'yes'){
        $(table+' tr:last').append('<td>' + self.runningTotal.toExponential(3) + '</td>');   // adds running LR column
      }
      } 
      else {  // imputes using impute_table in views.py and got_impute below
       
       $.get('/disease/impute_table/', {strongest_snp: v['strongest_snp'], pubmedid: v['pubmedid'], link: v['link'], disease_trait: v['disease_trait'], initial_sample_size: v['initial_sample_size'], reported_genes: v['reported_genes'], p_value_text: v['p_value_text'], or_or_beta: v['or_or_beta'], risk_allele: v['risk_allele'], p_value: v['p_value'], genotype: v['genotype']}, function(data){self.got_impute(data, table, view, object)}); 
      }
    });
    $(table).show();
    $(table).trigger('update');
    $('#looking-up').dialog('close');
    sorttable.innerSortFunction.apply($(table).find('th:first')[0], []);
    if (object.geneticRiskScore === 'yes'){
    $.get('/bmd2/', {population: get_user().population}, function(data){self.draw_chart(data, table, view, object)});
  }
  },

  got_impute: function(response, table, view, object) {
 
  	self = this;
    if (response != null) {
      response['strongest_snp'] = response['strongest_snp'] + "*";
      response['dbsnp'] = response['strongest_snp'];
      response['genotype'] = "??";
      user = get_user();
      var anchor_genotype = user.lookup(filter_identifier(response['anchor'])); // returns an object ex. {genotype: "CT"}

      if (anchor_genotype != undefined && response['phase'] != null && response['phase'] != 0 && response['refallele'] != null && response['otherallele'] != null) {
        response['genotype'] = "";
        if (response['phase'] == 1) { // if it is phase 1
          for (var i = 0; i < 2; i++) {
            if (anchor_genotype.genotype.charAt(i) == response['anchor_ref']) {
              response['genotype'] += response['refallele'].trim();
            } else {
              response['genotype'] += response['otherallele'].trim();
            }
          }
        } else {  // if it is phase 2
          for (var i = 0; i < 2; i++) {
            if (anchor_genotype.genotype.charAt(i) == response['anchor_ref']) {
              response['genotype'] += response['otherallele'].trim();
            } else {
              response['genotype'] += response['refallele'].trim();
            }
          }
        }
        if (response['risk_allele'] != "?" && !isNaN(response['or_or_beta'])) {
          this.runningTotal += calculate_lr(response['risk_allele'], response['genotype'], response['or_or_beta']); 
        }   
      } else if (response['freq'] != undefined) {  // if the anchor snp/genotype doesn't exist, uses population average  
        response['genotype'] = "??"
        if (response['risk_allele'] != "?" && !isNaN(response['or_or_beta'])) {
          this.runningTotal += 2 * response['freq'] * response['or_or_beta'];
        }
      }
      $(table + ' > tbody').append(_.template(view.disease_template, response));
     if (object.geneticRiskScore === 'yes'){
    $(table + ' tr:last').append('<td>' + self.runningTotal.toExponential(3) + '</td>');
    }
	object.running_total = self.runningTotal;
	}
  },

  //FUNCTIONS FOR GRS

  got_diseases_grs: function(response, table, view, object) {
    var self = this;
    user = get_user();
    self.runningTotal = 0;
  object.running_total = 0;
  
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['dbsnp']));
      if (dbsnp != undefined && dbsnp.genotype != "--"){ // if the user's genotype is defined at this snp
        v['genotype'] = dbsnp.genotype;  
      if(v['risk_allele'].charAt(0) !== "?") { 
        self.runningTotal += calculate_lr(v['risk_allele'], v['genotype'], v['beta']);   
      }
      $(table + ' > tbody').append(_.template(view.disease_template, v));
        $(table + ' tr:last').append('<td>' + self.runningTotal.toExponential(3) + '</td>');   // adds running LR column
      } else {  // imputes using impute_table in views.py and got_impute below
        
        $.get('/bmd2/impute_table/', {strongest_snp: v['dbsnp'], dbsnp: v['dbsnp'], genotype: v['genotype'], risk_allele: v['risk_allele'], beta: v['beta']}, function(data){self.got_impute(data, table, view, object)}); 
      }
      
    });
    $(table).show();
    $(table).trigger('update');
    $('#looking-up').dialog('close');

    //sorttable.innerSortFunction.apply($(table).find('th:contains("Score"):first')[0], []);
    //data: data from initial mysql query from bmd table (not needed)
    //table: html table from interpretome html.
    //view: the window for the table being drawn
    //object: js file attributes -> mysql search type, table types, add grs column etc.
    object.running_total = self.runningTotal;
    $.get('/bmd2/', {population: get_user().population}, function(data){self.draw_chart(data, table, view, object)});
    
  },


// response: stuff returned from mysql search on line 166

draw_chart: function(response, table, view, object) {
    data = object.data_table;
    running_total = Number(object.running_total);
//place your score into the histogram
    var x = data.getNumberOfRows() - 1;
    for (var i = 0; i < data.getNumberOfRows() - 1; i++) {
      if (running_total >= data.getValue(i, 0) && running_total < data.getValue(i+1, 0)) {
        x = i;
      }
    }
    data.addRow([data.getValue(x, 0), data.getValue(x, 1), 5]);

    var chart = new google.visualization.ColumnChart(     
      $(table).prevAll('div:first')[0]
    );

    chart.draw(data, {
      width: 800,
      height: 400,
      title: 'Population Scores',
      bar: { groupWidth: "95%" },
      legend: { position: 'right' },
      isStacked: true,
      tooltip: { trigger: 'none' },
    });
    
    $(table).prevAll('div:first').show();
  },


  click_disease: function(event, object, view) {
    var button = $(event.target).closest('button')  // get the button that was clicked
    var buttonId = button.attr('id'); // get the id of the button that was clicked
    $('#current-table').removeAttr('id'); //make sure that no other tables are marked as the table being currently modified

    //if the next table DOES NOT have a class that is the same as the id of the button, OR, if the table does not exist yet...
    if(!button.nextAll('table:first').hasClass(buttonId)){
      //insert a table with the skeleton  in interpretome.html immediately after the button
      if(object.typeOfTable === 'grs'){
        button.after($('#table-model-grs').html());
      } else if (object.typeOfTable === 'pvalue'){
        button.after($('#table-model-p').html());
      } else {
        button.after($('#table-model').html());
      }
      var createdTable = button.nextAll('table:first'); // set a variable with that table that was created
      createdTable.attr('id', 'current-table'); // set its id to mark it as the table currently being modified
      sorttable.makeSortable(createdTable[0]); // initialize tablesorter
      createdTable.addClass(buttonId);  // add class that's the same as button id so we know that they're associated w/ each other
    } else {
      // if the table already exists then mark it as the current table
      button.nextAll('.'+buttonId+':first').attr('id', 'current-table');
    }

    var table = '#current-table';
    var diseaseToGet = object.disease;
    var specificOrMultiple = object.specificOrMultiple;

    //find the template inside the current table
    view.disease_template = $('#current-table').find('#table-template').html();
    clear_table(table.replace("#", "").replace(".", ""));
    var self = this;

    this.disease = diseaseToGet;

    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    if (specificOrMultiple === 'specific' || specificOrMultiple === 'multiple') {
      $.get('/disease/get_gwas_' + specificOrMultiple, {population: get_user().population, disease: diseaseToGet}, function(data){self.got_diseases(data, table, view, object)});
    } else if (specificOrMultiple === 'grs') {
      $.get('/bmd2/', {population: get_user().population, category: object.category}, function(data){self.got_diseases_grs(data, table, view, object)});
    } else {
      $.get('/disease/get_gwas', {population: get_user().population, disease: diseaseToGet}, function(data){self.got_diseases(data, table, view, object)});
    }
  }

};



