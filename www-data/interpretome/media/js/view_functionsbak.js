//This is table filler app from AJ.
var view_functions = {

    click_button: function(event, view) {

      var self = view;
      var id = $(event.target).closest('button').attr('id');
      var obj;
      
      //the specific button is defined in the *.js file.
      for (disease in self.buttons) {
       
        if (!self.buttons.hasOwnProperty(disease)) {
          continue;
        }
        obj = self.buttons[disease];
        
        if (typeof self.buttons[disease] !== 'string' && obj.id === id)
        {
          this.click_disease(event, obj, self);
          break;
        }
    }

  },



    render: function(view) {
            var self = view;
            var it = this;
            $.get('/media/template/'+view.buttons.html+'.html', function(data){it.loaded(data, self)});
            $.get('/media/template/tablelegends.html', function(data){it.loaded(data, self)});
  },
    loaded: function(response, view) {
  
       
 $(view.el).append(response);
            
            if(!view.has_loaded){
                var sorted = [];
               
                $('button').button();
                match_style(view.el);
              
                view.has_loaded = true;
          }
  },

    got_diseases: function(response, table, view) {
           var self = view;
           user = get_user();
           $.each(response, function(i, v) {
           var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
           
           if (dbsnp != undefined){
             v['genotype'] = dbsnp.genotype;
             $(table+' > tbody').append(_.template(self.disease_template, v));
          }
          else {
             v['genotype'] = '??';
             $(table+' > tbody').append(_.template(self.disease_template, v));
          }});
          $(table).show();
          $(table).trigger('update');
          
          $('#looking-up').dialog('close');

          //sort tablesorter on column 0
          $(table).trigger('sorton', [ [[0,0]] ]);        
  },

    click_disease: function(event, object, view) {
            //get the button that was clicked
            var button = $(event.target).closest('button')
            //get the id of the button that was clicked
            var buttonId = button.attr('id');

            //make sure that no other tables are marked as the table being currently modified
            $('#current-table').removeAttr('id');

            //if the next table DOES NOT have a class that is the same as the id of the button, OR, if the table does not exist yet...
            if(!button.nextAll('table:first').hasClass(buttonId)){
              //insert a table with the skeleton  in interpretome.html immediately after the button
              button.after($('#table-model').html());
              //set a variable with that table that was created
              var createdTable = button.nextAll('table:first');
            
              //set its id to mark it as the table currently being modified
              createdTable.attr('id', 'current-table');

              //initialize tablesorter
              createdTable.tablesorter();

              //add class that's the same as button id so we know that they're associated w/ each other
              createdTable.addClass(buttonId);
            }
            else
            {
              // if the table already exists then mark it as the current table
              button.nextAll('.'+buttonId+':first').attr('id', 'current-table');
            }


            var table = '#current-table';

            //get the disease to be searched from the object in the view
            var diseaseToGet = object.disease;
            var specificOrMultiple = object.specificOrMultiple;

            //find the template inside the current table
            view.disease_template = $('#current-table').find('#table-template').html();
            clear_table(table.replace("#", "").replace(".", ""));
            var self = this;
            if (window.App.check_all() == false) return;
            $('#looking-up').dialog('open');
            if (specificOrMultiple === 'specific' || specificOrMultiple === 'multiple' || specificOrMultiple === 'multiply')
            {
              $.get('/disease/get_gwas_' + specificOrMultiple, {population: get_user().population, disease: diseaseToGet}, function(data){self.got_diseases(data, table, view)});
            }
            else{
              $.get('/disease/get_gwas', {population: get_user().population, disease: diseaseToGet}, function(data){self.got_diseases(data, table, view)});
            }




  }
};


var view_functions_old = {

    click_button: function(event, view) {

      var self = view;
      var id = $(event.target).closest('button').attr('id');
      var obj;
      
      for (disease in self.buttons) {
        //console.log(typeof self.buttons[disease] === 'string');
        if (!self.buttons.hasOwnProperty(disease)) {
          continue;
        }
        obj = self.buttons[disease];
        
        if (typeof self.buttons[disease] !== 'string' && obj.id === id)
        {
          view_functions.click_disease(event, obj, self);
          break;
        }
    }

  },



    render: function(view) {
            var self = view;
            $.get('/media/template/'+view.buttons.html+'.html', function(data){view_functions.loaded(data, self)});
            $.get('/media/template/tablelegends.html', function(data){view_functions.loaded(data, self)});
  },
    loaded: function(response, view) {
  
          

            
            if(!view.has_loaded){
                var sorted = [];
                $(view.el).append(response);
                $('button').button();
                match_style(view.el);
                for (disease in view.buttons) {
                  if (!view.buttons.hasOwnProperty(disease)) {
                    continue;
                  }
                  obj = view.buttons[disease];
                  
                  if (typeof obj !== 'string' && sorted.indexOf(obj.table)===-1)
                  {
                    //$(obj.table).hide();
                    $(obj.table).tablesorter();
                    sorted.push(obj.table);
                    console.log(sorted);

                  }
                }
              view.has_loaded = true;
          }
  },

    got_diseases: function(response, table, view) {
           var self = view;
           user = get_user();
           $.each(response, function(i, v) {
           var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
           if (dbsnp != undefined){
             v['genotype'] = dbsnp.genotype;
             $(table+' > tbody').append(_.template(self.disease_template, v));
          }
          else {
             v['genotype'] = '??';
             $(table+' > tbody').append(_.template(self.disease_template, v));
          }});
          $(table).show();
          $(table).trigger('update');
          
          $('#looking-up').dialog('close');
          $(table).trigger('sorton', [ [[0,1]] ]);        
  },

    click_disease: function(event, object, view) {

            var table = object.table;
            var diseaseToGet = object.disease;
            var specificOrMultiple = object.specificOrMultiple;
            view.disease_template = $(object.template).html();
            clear_table(table.replace("#", "").replace(".", ""));
            var self = this;
            if (window.App.check_all() == false) return;
            $('#looking-up').dialog('open');
            if (specificOrMultiple === 'specific' || specificOrMultiple === 'multiple' || specificOrMultiple === 'multiply')
            {
              $.get('/disease/get_gwas_' + specificOrMultiple, {population: get_user().population, disease: diseaseToGet}, function(data){self.got_diseases(data, table, view)});
            }
            else{
              $.get('/disease/get_gwas', {population: get_user().population, disease: diseaseToGet}, function(data){self.got_diseases(data, table, view)});
            }




  }
};

var view_functions_table = {

    click_button: function(event, view) {

      var self = view;
      var id = $(event.target).closest('button').attr('id');
      var obj;
      
      for (disease in self.buttons) {
        //console.log(typeof self.buttons[disease] === 'string');
        if (!self.buttons.hasOwnProperty(disease)) {
          continue;
        }
        obj = self.buttons[disease];
        
        if (typeof self.buttons[disease] !== 'string' && obj.id === id)
        {
          this.click_disease(event, obj, self);
          break;
        }
    }

  },



    render: function(view) {
            var it = view;
            var self = this;
            $.get('/media/template/'+view.buttons.html+'.html', function(data){self.loaded(data, it)});
            $.get('/media/template/tablelegends.html', function(data){self.loaded(data, it)});
  },
    loaded: function(response, view) {
  
          

            var sorted = [];
            console.log(view.buttons.html);
            $(view.el).append(response);
            $('button').button();
            if(!view.has_loaded){
                //$('.added').trigger('destroy', [false, function(table){$('.added').tablesorter();}]);
                match_style(view.el);
                $('.table-adder:not(.done)').append($('#table-model').html());
                $('.table-adder:not(.done)').addClass('done');
                $('.unadded').toggleClass("added unadded");
                //$('.added').hide();
                $('.added:not(.sorted)').tablesorter();
                $('.added:not(.sorted)').addClass('sorted');


/*
                for (disease in view.buttons) {
                  if (!view.buttons.hasOwnProperty(disease)) {
                    continue;
                  }
                  obj = view.buttons[disease];
                  
                  if (typeof obj !== 'string' && sorted.indexOf(obj.table)===-1)
                  {
                    $(obj.table).hide();
                    $(obj.table).tablesorter();
                    sorted.push(obj.table);
                    console.log(sorted);

                  }
                }
                */
              view.has_loaded = true;
          }
  },

    got_diseases: function(response, table, view) {
           var self = view;
           user = get_user();
           $.each(response, function(i, v) {
           var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
           if (dbsnp != undefined){
             v['genotype'] = dbsnp.genotype;
             $(table+' > tbody').append(_.template(self.disease_template, v));
          }
          else {
             v['genotype'] = '??';
             $(table+' > tbody').append(_.template(self.disease_template, v));
          }});
          $(table).show();
          $(table).trigger('update');
          
          $('#looking-up').dialog('close');
        
          $(table).trigger('sorton', [ [[0,1]] ]);   
          

  },

    click_disease: function(event, object, view) {

            var table = object.table;
            $('#current-table').removeAttr('id');
            table = '#' + $('#' + object.id).closest('div').find('.added').attr('id', 'current-table').attr('id');
            console.log(table);
            var diseaseToGet = object.disease;
            var specificOrMultiple = object.specificOrMultiple;
            view.disease_template = $(table).find('#table-template').html();
            clear_table(table.replace("#", "").replace(".", ""));
            var self = this;
            if (window.App.check_all() == false) return;
            $('#looking-up').dialog('open');
            if (specificOrMultiple === 'specific' || specificOrMultiple === 'multiple' || specificOrMultiple === 'multiply')
            {
              $.get('/disease/get_gwas_' + specificOrMultiple, {population: get_user().population, disease: diseaseToGet}, function(data){self.got_diseases(data, table, view)});
            }
            else{
              $.get('/disease/get_gwas', {population: get_user().population, disease: diseaseToGet}, function(data){self.got_diseases(data, table, view)});
            }




  }
};


