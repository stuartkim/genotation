var url;
var page="";
var hashIndex;

window.AppController = Backbone.Router.extend({
  
  routes: {



    
    'start': 'start',
    'lookup': 'lookup',
    'explore': 'explore',
    'squish': 'squish'

    
    
    /*'warfarin': 'warfarin',
    'diabetes': 'diabetes',
    'disease': 'disease',
    'amd': 'amd',
    'hello': 'hello',
    
    'osteoarthritis': 'osteoarthritis',
    'cancers': 'cancers',
    
    'bipolar': 'bipolar',
    'celiac': 'celiac',
    'depression': 'depression',
    'disc': 'disc',
    'eso': 'eso',
    'graves': 'graves',
    'asthma': 'asthma',
    'colorectal': 'colorectal',
    'migraine': 'migraine',
    'pku': 'pku',
    'platelet': 'platelet',
    'qt': 'qt',
    'fabian': 'fabian',
    'bmd': 'bmd',
    'vitiligo': 'vitiligo',
    // 'sickle_cell_trait': 'sickle_cell_trait',
    'pharmacogenomics': 'pharmacogenomics',
    
    'similarity': 'similarity',
    'pca': 'pca',
    'painting': 'painting',
    'family': 'family',
    'phasing': 'phasing',
    
    
    'baldness': 'baldness',
    'coffee': 'coffee',
    'caffeine': 'caffeine',
    'tan': 'tan',
    'eqtl': 'eqtl',
    'gwas': 'gwas',
    'height': 'height',
    'longevity': 'longevity',
    'neandertal': 'neandertal',
    'selection': 'selection',
    'terms': 'terms'*/
  },
  
  squish: function(boolean, i, j)
  {
    //routes["'" + arrayList[1][0] "'"] = arrayList[1][0];
    //console.log("hello");
    url = window.location.href;
    hashIndex = url.indexOf("#");
    //console.log(page.length -)
    if(!boolean && hashIndex > 0)
    {
        for(var k = hashIndex+1; k < url.length; k++)
        {
            page += url.charAt(k);
        }
        outerloop:
        for (var k = 0; k < arrayList.length; k++)
        {
            for (var l = 0; l < arrayList[k].length; l++)
            {
                if(arrayList[k][l] === page)
                {

                  funcsList[k][l]();
                  isRendered[k][l]=true;
                  break outerloop;
                }
            }
        }
        

    }

    if (boolean)
    {
       funcsList[i][j]();
    }


  },

  start: function() {
    this.render_or_show(window.Start);
  },
  
  lookup: function() {
    this.render_or_show(window.Lookup);
  },
  
  explore: function() {
    this.render_or_show(window.Explore);
  },
  /*
  diabetes: function() {
    this.render_or_show(window.Diabetes);
  },
  disease: function() {
    this.render_or_show(window.Disease);
  },
  amd: function() {
    this.render_or_show(window.Amd);
  },
  osteoarthritis: function() {
    this.render_or_show(window.Osteoarthritis);
  },
  cancers: function() {
    this.render_or_show(window.Cancers);
  },
  bipolar: function() {
    this.render_or_show(window.BipolarDisorder);
  },
  celiac: function() {
    this.render_or_show(window.Celiac);
  },
  depression: function() {
    this.render_or_show(window.Depression);
  },
  disc: function() {
    this.render_or_show(window.Disc);
  },
  eso: function() {
    this.render_or_show(window.Eso);
  },
  graves: function() {
    this.render_or_show(window.Graves);
  },
  asthma: function() {
    this.render_or_show(window.Asthma);
  },
  colorectal: function() {
    this.render_or_show(window.Colorectal);
  },
  migraine: function() {
    this.render_or_show(window.Migraine);
  },
  hello: function() {
    this.render_or_show(window.Hello);
  },
  pku: function() {
    this.render_or_show(window.Pku);
  },
  platelet: function() {
    this.render_or_show(window.Platelet);
  },
  qt: function() {
    this.render_or_show(window.Qt);
  },
  fabian: function() {
    this.render_or_show(window.Fabian);
  },
  bmd: function() {
    this.render_or_show(window.Bmd);
  },
  vitiligo: function() {
    this.render_or_show(window.Vitiligo);
  },
  //  sickle_cell_trait: function() {
   //  this.render_or_show(window.SickleCellTrait);
    // },
  warfarin: function() {
    this.render_or_show(window.Warfarin);
  },
  pharmacogenomics: function() {
    this.render_or_show(window.Pharmacogenomics);
  },
  
  similarity: function() {
    this.render_or_show(window.Similarity);
  },
  pca: function() {
    this.render_or_show(window.Pca);
  },
  painting: function() {
    this.render_or_show(window.Painting);
  },
  family: function() {
    this.render_or_show(window.Family);
  },
  phasing: function() {
    this.render_or_show(window.Phasing);
  },
  
  baldness: function() {
    this.render_or_show(window.Baldness);
  },
  coffee: function() {
    this.render_or_show(window.Coffee);
  },
  caffeine: function() {
    this.render_or_show(window.Caffeine);
  },
  tan: function() {
    this.render_or_show(window.Tan);
  },
  eqtl: function() {
    this.render_or_show(window.Eqtl);
  },
  gwas: function() {
    this.render_or_show(window.Gwas);
  },
  height: function() {
    this.render_or_show(window.Height);
  },
  longevity: function() {
    this.render_or_show(window.Longevity);
  },
  neandertal: function() {
    this.render_or_show(window.Neandertal);
  },
  selection: function() {
    this.render_or_show(window.Selection);
  },*/



  
  terms: function() {
    this.render_or_show(window.Terms);
  },

  


  render_or_show: function(controller) {
    if (controller.has_loaded) {
      $('#tabs').tabs('select', '#' + $(controller.el).attr('id'));
    } else {
      controller.render();
    }
  }
});

