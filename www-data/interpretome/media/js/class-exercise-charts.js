'use strict';
/* JS for Class Exercise Charts */
$(document).ready(function() {

    $('#chart-form').submit(function(evt) {   
		evt.preventDefault()
        var exercise = $("select#exercise").val();
	
		// hide and remove current charts from page
		$('.chart').fadeOut("slow", function() {
			$('#charts').empty();
		});
		
		// get JSON for given exercise   
        $.getJSON("/exercise_charts?exercise=" + exercise).done(function(data) {
        		
            // get allele freq formatted
            var prop;
        	var exerciseObj = data;
            var snpsObj = exerciseObj.allele_frequency;
            var haplotypesObj = exerciseObj.haplotypes;
            var snpList = [];
            
            
            //$.each( snpsObj, function( key, value ) {
			//  snpList.push(prop);
			//});
			

            for (prop in snpsObj) {
				if (!snpsObj.hasOwnProperty(prop)) {
					//The current property is not a direct property of p
					continue;
				}
				//create array of keys (i.e. snps) from returned allele freq obj
				snpList.push(prop);
			}
			
			console.log("Snps? " + JSON.stringify(snpsObj[1]));
		    
			console.log(snpList);
			// transform genotypeToAlleleFreq
        	for (var i = 0; i < snpList.length; i++) {
    			//alert(snpsObj.snpList[i]);
    			//Do something
			}
                
                
            }).fail(function(jqxhr, textStatus, error) {
            	//debugger;
				var err = textStatus + ', ' + error;
				console.log("getJSON Request Failed: " + err);
        	});
    });
});


function renderChart(data) {
	// pick which type of chart to draw, and draw it
}

function getNextChartId() {
	// find charts already on page (by id), spit back next usable id
	var max = 0;
	var nextId = 0;
	var num;
	$(".chart").each(function() {
    	num = parseInt(this.id.split("-")[1], 10);
    	if (num > max) {
        	max = num;
    	}
	});
	nextId = max + 1;
	//console.log("from getNextChartId, " + nextId);
	return nextId;
}

function createChartDiv() {
	var chartDivId = getNextChartId();
	//console.log("From createChartDiv, " + chartDivId);
	$("#charts").append("<section class='chart' id=chart-" + getNextChartId() + "></section>");
	return chartDivId;
}

function createAlleleFreqChart(allele1, freq1, allele2, freq2, snp) {
	var chartId = createChartDiv();
	//console.log("From createAlleleFreqChart; id to be used = " + chartId);
	$('#chart-' + chartId).highcharts({
		chart: {
			type: 'bar',
			borderWidth: 3,
			borderColor: '#eaeaea',
			width: 700,
			margin: 60,
		},
		title: {
			text: 'Class Allele Frequecy for SNP ' + snp,
			style: {
				fontWeight: 'bold',
				fontSize: '1.8em',
			}
		},
		xAxis: {
			categories: [allele1, allele2],
			title: {
				text: 'Alleles',
				style: {fontSize: '1.2em'},
			},
			labels: {
				style: {fontWeight: 'bold', fontSize: '1.4em'},
			},
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Frequency',
				align: 'middle',
				style: {fontSize: '1.2em'},
				margin: 20,
			},
			labels: {
				overflow: 'justify'
			}
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true,
					style: {fontWeight: 'bold', fontSize: '1.2em'}
				}
			}
		},
		legend: {
			enabled: false,
		},
		series: [{
			name: 'Alleles',
			data: [freq1, freq2]
		}]
	});
}


function genotypeToAlleleFreq(o) {
    // converts object from format: 
    //     {"CC" : 15, "CT" : 10, "TT" : 5 }
    // to:
    //     { "C": 40, "T" : 20 }
    // IE < 9 compatible?  See MDN on Oject.keys
    // http://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/keys#Compatibility
    var result = {};
    var i, j;
    for (var i = 0; i < Object.keys(o).length; i += 1) {
        str = Object.keys(o)[i];
        for (var j = 0; j < str.length; j += 1) {
            var value = 0;
            if (result.hasOwnProperty(str[j]))
                value = result[str[j]];
            result[str[j]] = value + o[str];
        }
    }
    return result;
}
/*
var obj={
   "CC" : 23,
   "CT" : 36,
   "TT" : 12,
};

console.log(genotypeToAlleleFreq(obj));
*/

function createHaplotypeTable( a, c) {
	// create a table of haplotypes with obs and expected values
}