$(function() {
window.CervicalView = Backbone.View.extend({
  has_loaded: false,
  el: $('#cervical'),

  
  
  events: { 'click #cervical_cancer-get-gwas': 'click_cervical_cancer'
, 'click #cervical_cancer-get-gwas': 'click_cervical_cancer'
},


  initialize: function() {
    _.bindAll(this, 'loaded', 'click_cervical_cancer', 'got_cervical_cancer'
, 'click_cervical_cancer', 'got_cervical_cancer'
);
  },
  
  render: function() {
    $.get('/media/template/cervical.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#cervical_cancer-table').hide();
	  this.disease_template = $('#cervical_cancer-template').html();
	  match_style(this.el);
    $('#cervical_cancer-table').tablesorter({ 
        sortList: [[TABLE_SORT_COLUMN,TABLE_SORT_ORDER]] 
    });
    this.has_loaded = true;
  }
,
  
  got_cervical_cancer: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#cervical_cancer-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#cervical_cancer-table').show();
    $('#cervical_cancer-table').trigger('update');
    $('#looking-up').dialog('close');
  }
  
,


  click_cervical_cancer: function(event) {
    clear_table('cervical_cancer-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'cervical_cancer_SPACE'}, this.got_cervical_cancer);
  }
});

});
