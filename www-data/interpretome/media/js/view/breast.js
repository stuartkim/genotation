$(function() {
window.BreastView = Backbone.View.extend({
  has_loaded: false,
  el: $('#breast'),

  
  
  events: { 'click #breast_cancer-get-gwas': 'click_breast_cancer'
, 'click #breast_cancer-get-gwas': 'click_breast_cancer'
},


  initialize: function() {
    _.bindAll(this, 'loaded', 'click_breast_cancer', 'got_breast_cancer'
, 'click_breast_cancer', 'got_breast_cancer'
);
  },
  
  render: function() {
    $.get('/media/template/breast.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#breast_cancer-table').hide();
	  this.disease_template = $('#breast_cancer-template').html();
	  match_style(this.el);
    $('#breast_cancer-table').tablesorter({ 
        sortList: [[TABLE_SORT_COLUMN,TABLE_SORT_ORDER]] 
    });
    this.has_loaded = true;
  }
,
  
  got_breast_cancer: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#breast_cancer-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#breast_cancer-table').show();
    $('#breast_cancer-table').trigger('update');
    $('#looking-up').dialog('close');
  }
  
,


  click_breast_cancer: function(event) {
    clear_table('breast_cancer-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'breast_cancer_SPACE'}, this.got_breast_cancer);
  }
});

});
