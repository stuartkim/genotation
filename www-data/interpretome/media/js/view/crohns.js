$(function() {
window.CrohnsView = Backbone.View.extend({
  has_loaded: false,
  el: $('#crohns'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/crohns.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#crohns-table').hide();
	  this.disease_template = $('#crohns-template').html();
	  match_style(this.el);
    $('#crohns-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    var runningTotal = 0;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#crohns-table > tbody').append(_.template(self.disease_template, v));

        // CALCULATES LR
        if(v['risk_allele'].charAt(0) !== "?") { 
          var match = 0;
          for (var i = 0; i < v['genotype'].length; i++) {
            if (v['risk_allele'].charAt(0) === v['genotype'].charAt(i)) {
              match++;
            }
          }
          runningTotal += parseFloat(v['or_or_beta']) * match;
        }

        $('#crohns-table tr:last').append('<td>' + runningTotal.toFixed(2) + '</td>');
      }
    });
    $('#crohns-table').show();
    $('#crohns-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('crohns-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Bone mineral density'}, this.got_diseases); // CHANGE!!!!!
  }

  });
});