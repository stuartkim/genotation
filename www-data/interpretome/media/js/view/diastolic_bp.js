$(function() {
window.DiastolicBPView = Backbone.View.extend({
  has_loaded: false,
  el: $('#diastolic_bp'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/diastolic_bp.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#diastolic_bp-table').hide();
	  this.disease_template = $('#diastolic_bp-template').html();
	  match_style(this.el);
    $('#diastolic_bp-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    var runningTotal = 0;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#diastolic_bp-table > tbody').append(_.template(self.disease_template, v));

        // CALCULATES LR
        if(v['risk_allele'].charAt(0) !== "?") { 
          var match = 0;
          for (var i = 0; i < v['genotype'].length; i++) {
            if (v['risk_allele'].charAt(0) === v['genotype'].charAt(i)) {
              match++;
            }
          }
          runningTotal += parseFloat(v['or_or_beta']) * match;
        }

        $('#diastolic_bp-table tr:last').append('<td>' + runningTotal.toFixed(2) + '</td>');
      }
    });
    $('#diastolic_bp-table').show();
    $('#diastolic_bp-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('diastolic_bp-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Bone mineral density'}, this.got_diseases); // CHANGE!!!!!
  }

  });
});