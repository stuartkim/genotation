$(function() {
window.SelectionView = Backbone.View.extend({
  el: $('#selection'),
  name:'Selection',  
  table_id: '#selection_table',
  template_id: '#selection_template',
  url: '/media/template/selection.html',
  help_url: '/media/help/selection.html',
  has_loaded: false,

  events: {
    'click #predict_selection': 'click_selection_snps',
    'click #submit-selection': 'click_submit',
    'click #confirm-submit-snps': 'click_confirm_submit'
  },
  
  initialize: function() {
    _.bindAll(this, 'loaded',
      'click_selection_snps',
      'click_submit',
      'click_confirm_submit',
      'display',
      'finish'
    );
  },
  
  render: function() {
    $.get('/media/template/selection.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
	  $('.help-button').button({
      icons: {primary: 'ui-icon-help'}	    
	  });
	  $('.help > div').show();
	  $('.description > div').hide();
	  $('#predict_selection').button();
	  $('#submit-selection').button({disabled:true});
	  this.table_template = $(this.template_id).html();
	  this.selection_template = $('#selection-template').html(); //added
	  this.has_loaded = true;
  },
  
  click_selection_snps: function(response) {
    if (window.App.check_all() == false) return;
    var dbsnps = filter_identifiers(
      $('#selection-snps-textarea').val().split('\n')
    );
    var self=this;
    $.get(
      '/selection/get_selection_snps/', {
        population: user.population
      }, function(response) {
        return user.lookup_snps(
          self.display, response, _.keys(response), {}
        );
      }
    );
  },
  
  click_submit: function(event) {
    var self = this;
    $('#confirm-submit-exercise').dialog({
      modal: true, resizable: false, buttons: {
        'Confirm' : function() {
	  //console.log('blah');
          self.click_confirm_submit();
          $(this).dialog('close');
        },
        'Cancel': function() {$(this).dialog('close');}
      }
    });
  },
  
  click_confirm_submit: function(event) {
    var output = {};
    var self = this;
    output['total_selection_index']=$('#total_selection_index').html();
    output['total_selection']=$('#total_selection').html();
    output['stuart_cant_divide']=$('#stuart_cant_divide').html();
    output['exercise']='selection';
    output['population']=get_user().population;
    //console.log(output)
//    $('#neandertal_table td').each( function() {
//	output.push(this.innerText.split(' ')[0])
//      }
//    );
//    console.log(this.table_id)
//    data={neandertal:output.join()}
//    console.log(data)
    $.get( 'submit', output, check_submission);
  },
  
  display: function(response, all_dbsnps, extended_dbsnps) {
    var self = this;
    
    var n_selected = 0;
    var n_derived = 0;
    var total = 0;
    //console.log(response)
    $.each(response, function(i, v) {
      var snp = extended_dbsnps[i];
      _.extend(v, extended_dbsnps[i]);
      $(self.table_id + " > tbody").append(_.template(self.table_template, v));
      
      if (v['genotype'] != '??'){
        var count = count_genotype(v['genotype'], v['ancestral']);
        n_derived += (2 - count);
        var count = count_genotype(v['genotype'], v['selected']);      
        n_selected += count;
        total += 2;
      }
    });
    $('#total_selection_index').html(n_selected);
    $('#total_selection').html(total);
    var stuart_divide=Math.round(n_selected/total*1000)/1000
    $('#stuart_cant_divide').html(stuart_divide);
    $('#count_selection').show()
    $(this.table_id).show();
    $(this.table_id).tablesorter();
    $('#table-options').show();
    $("#submit-selection").button("option", "disabled", false);
    this.finish(n_selected, n_derived, total, stuart_divide);
  },
  
  finish: function(n_selected, n_derived, total, stuart_divide) {
    data = new google.visualization.DataTable();
    
    data.addColumn('string', 'Label');
    data.addColumn('number', 'Value');
    data.addRows(2);
    data.setValue(0, 0, 'Derived');
    data.setValue(0, 1, n_derived);
    data.setValue(1, 0, 'Selected');
    data.setValue(1, 1, n_selected);
    
    var chart = new google.visualization.Gauge(document.getElementById('selection_chart'));
    var options = {width: 900, height: 300, redFrom: total/2, redTo: total,
        min: 0, max: total,
        yellowFrom:total/4, yellowTo: total/2, minorTicks: 5};
    chart.draw(data, options);
    
    $('#selection_chart').show();
    
  }
});
});
