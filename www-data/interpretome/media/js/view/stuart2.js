$(function() {
window.Stuart2View = Backbone.View.extend({
  has_loaded: false,
  el: $('#stuart2'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/stuart2.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#stuart2-table').hide();
	  this.disease_template = $('#stuart2-template').html();
	  match_style(this.el);
    $('#stuart2-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#stuart2-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#stuart2-table').show();
    $('#stuart2-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('stuart2-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'osteoarthritis'}, this.got_diseases);
  }
  });
});
