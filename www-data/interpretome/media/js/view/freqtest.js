$(function() {
window.FreqtestView = Backbone.View.extend({
  //don't change these
  has_loaded: false,
  el: $('#freqtest'),
  snps: {},
  snps_array: [],
  snps_string: '',
  people: {},
  people_array: [],

  //change these
  column: 'strongest_snp',
  table: 'interpretome_clinical.gwas_catalog',
  people_number: 100,

  buttons: {

    html: 'freqtest',

  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button', 'add_rsid', 'add_rsid_actually', 'add_people', 'add_person', 'add_table', 'update_people2');

  },
  
  render: function() {
    view_functions.render(this);
    
  },
    

  click_button: function(event) {

      var self = this;
      $('#looking-up2').dialog('open');
     //go to allele_freqs in views.py. store every snp into snps array
      $.get('/lookup/get_allele_freqs', {population: 'CEU', column: self.column, table:self.table}, function(data){
    
        
        var snps_array = [];
        $.each(data, function(i, v) {
          if(v['strongest_snp'].match(/^\d+$/)){
           snps_array.push(v['strongest_snp']);
        }
      });

        var snps_string = snps_array.join();
        self.snps_string = snps_string;
        //this gets the allele freqs for every SNP from mysql table.
        //console.log(snps_string);
        $.get('/lookup/get_allele_frequencies1', {population: 'CEU', snps:snps_string}, function(data){

        self.add_rsid(data)});
        });

  },

  add_table_rsid: function()
  {

  },

  add_table: function()
  {
    var self = this;
    $('#freq').find('tr').each(function(){
           var trow = $(this);
             if(trow.is('#head')){
                 trow.append('<th>rsid</th>');
             }else if (trow.is('#body')){
                
             }
            
         });
    for (dbsnp in self.snps)
    {
      $('#freq').append('<tr id = "'+dbsnp+'"><td>'+dbsnp+'</td></tr>');
    }
    var i = 0;
    for(person in self.people)
    {
      
      $('#head').append('<th>'+person+'</th>');
      //for(snp in self.people[person])
      //{
        //i++;
        //console.log(snp);
        //setTimeout(add_row, 0, snp, person, i);
      //}


    }
  },

  add_row: function(snp, person, i)
  {

      var percent = Math.round(i / Object.keys(response).length * 100);
          //console.log(Math.round(i / Object.keys(response).length * 100));
          if (percent < 100) {
                //console.log('set progressbar');
                $('#loading-bar2').progressbar('option', 'value', percent);
            }
      $('#freq').find('tr #' + snp + ':first').append('<td>'+self.people[person][snp]['allele']+'</td>');
  },

  // goes to add_person
  add_people: function()
  {
      
      var self = this;
      for (var i = 0; i < self.people_number; i++)
      {
          setTimeout(self.add_person, 0, i);
 
      }

  },

  // this gets the fake genotype for a person.  2x for each person.
  add_person: function(i)
  {
     var self = this;
     var percent = Math.round(i / self.people_number * 100);
         
      if (percent < 100) {
        $('#loading-bar2').progressbar('option', 'value', percent);
      }
        
        var letter = '';
        for(var j = 0; j < 2; j++)
        {
          if(j == 0)
          { letter = 'A'; }
          else
          { letter = 'B'; }
          var number = i.toString();
          
          self.people['person' + number + "_" + letter] = {};
          var obj = self.people['person' + number + "_" + letter];
          //people_array.push('person' + number + "_" + letter);

          var rand = Math.random();
         
          for (boo in self.snps){
            
            if(rand<parseFloat(self.snps[boo].refallele_freq))
            {
                //use refallele
                obj[boo] = {allele:self.snps[boo].refallele};
                
            }
            else{
                //use otherallele
                obj[boo] = {allele:self.snps[boo].otherallele};
               
            }
            
          }
             
      }

      if (i == self.people_number-1)
          {
              $('#looking-up2').dialog('close');
              $('#loading-bar2').progressbar('option', 'value', 0);
              $.get('/lookup/add_people_snps', {population: 'CEU', snps: self.snps_string}, function(data){self.update_people2()});
              
          } 
      },

  //this adds the genotypes as new columns in the people mysql table.
  update_people2: function()
  {
    var self = this;
    console.log(self.people);
    var genotype_array = [];
    
      for (person in self.people){
        genotype_array = [];
        self.people_array.push(person);
        for(dbsnp in self.people[person])
        {
            genotype_array.push(dbsnp.toString());
            genotype_array.push(self.people[person][dbsnp]['allele']);
        }

        $.get('/lookup/update_people', {population: 'CEU', person: person, genotype: genotype_array.join()}, function(){
            
        });
        console.log(person);
            if (person === 'person'+self.people_number-1+"_B")
            {
              console.log(done);
            }
      }  
      },
      
  // this increments the loading bar. 
  add_rsid: function(response)
  {
    console.log(response);
    var length = Object.keys(response).length;
    var self= this;
    $('.progress-bar').progressbar({value: 0});
    $('.progress-bar > div').css('background', get_secondary_color());
    var i = 0;
      for (v in response) {
          i++;
          setTimeout(self.add_rsid_actually, 0, response, v, i, length);
 
    }     

  },

  //this adds the rsid and gets the allele freqs.
  add_rsid_actually: function(response, v, i, l)
  {
    var self = this;
          var percent = Math.round(i / l * 100);
         
          if (percent < 100) {
                
                $('#loading-bar2').progressbar('option', 'value', percent);
            }
    
    var dbsnp = v;
    
    
           if (dbsnp != undefined && dbsnp != null && response[dbsnp] != null && response[dbsnp] != undefined){
            var ra = response[dbsnp]['refallele'];
            var raf = response[dbsnp]['refallele_freq'];
            var of = response[dbsnp]['otherallele'];
            
             self.snps[dbsnp] = {dbsnp: v, refallele: ra, refallele_freq: raf,
             otherallele: of};
             
          }

              if(i == l)
              {
                  $('#loading-bar2').progressbar('option', 'value', 0);
                  //$('#looking-up2').dialog('close');
                  //console.log(self.snps);
                  self.add_people();

              }
      

  }});

});