//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.IronView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#iron'),

  buttons: {

    html: '/vitamins/iron',

    amd: {
        id: 'get-gwas',
        //search term in disease_trait
        disease: 'iron status biomarkers',
        //type of table: OR, beta, p-value_text
        
        //type specific or multiple or pvalue(measurement) in the quotes if needed.
        specificOrMultiple: 'pvalue'

    }

  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});