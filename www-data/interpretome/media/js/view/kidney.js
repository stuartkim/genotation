$(function() {
window.KidneyView = Backbone.View.extend({
  has_loaded: false,
  el: $('#kidney'),

  
  initialize: function() {
    _.bindAll(this, 'loaded');
  },
  
  render: function() {
    $.get('/media/template/kidney.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  
	  match_style(this.el);
  
    this.has_loaded = true;
  },
  
  
  });
});
