$(function() {
window.TraitsView = Backbone.View.extend({
  has_loaded: false,
  el: $('#traits'),

  
  
  events: {
  'keyup': 'change_search', //does change_search button whenever keyup even is done
  'click #sort': 'click_sort',
  'click .link2': 'click_link'
},


  initialize: function() {
    _.bindAll(this, 'loaded', 'change_search', 'click_sort');
  },
  
  render: function() {
    $.get('/media/template/traits.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();


    //change #sort button css when hovered.
    $("#sort").hover(function(){
      $(this).css("background-color", "#e0e0e0");
      $(this).css("border-bottom-width", "2px");
      $(this).css("padding-bottom", "3px");
      $(this).css("cursor", "pointer");
    }, function(){
      $(this).css("background-color", "white");
      $(this).css("border-bottom-width", "1px");
      $(this).css("padding-bottom", "4px");
    });

  
	  match_style(this.el);
    this.has_loaded = true;
  }
,


  change_search: function(event) {
    //set variable searchTerm to whatever was typed into the box
    console.log($("#traits-search").is(":focus"));
    if(window.location.hash.substr(1) === 'traits' && $("#traits-search").is(":focus")){
    var daSearchTerm = $('#traits-search').val(); 


    //for everything in clinicalList...
     for(var i =0; i < traitsList.length; i++)
     {
      
        //hide if searchTerm is not found and show if it is found
        var nickIndex = hasNicknameList.indexOf(traitsList[i]);
        if (nickIndex >-1)
        {

          if(nicknameList[nickIndex].toLowerCase().indexOf(daSearchTerm.toLowerCase())===-1){
              $("."+hasNicknameList[nickIndex]).hide();
          }
          if(nicknameList[nickIndex].toLowerCase().indexOf(daSearchTerm.toLowerCase())>-1)
          {
            $("."+hasNicknameList[nickIndex]).show();
          }
            
        }
        else{
        if(traitsList[i].toLowerCase().indexOf(daSearchTerm.toLowerCase())===-1){
            $("."+traitsList[i]).hide();
        }
        if(traitsList[i].toLowerCase().indexOf(daSearchTerm.toLowerCase())>-1)
        {
          $("."+traitsList[i]).show();
        

        }
      }
     }
     //also use nicknames
   /* for(var i =0; i < nicknameList.length; i++)
     {
              if(nicknameList[i].toLowerCase().indexOf(daSearchTerm.toLowerCase())===-1){
            $("#"+hasNicknameList[i]).hide();
        }
        if(nicknameList[i].toLowerCase().indexOf(daSearchTerm.toLowerCase())>-1)
        {
          $("#"+hasNicknameList[i]).show();
          console.log(nicknameList[i]);

        }
     }*/

     //show the "Nothing found" text if the .clinical-buttons class contains no elements which have property display:none
     if( $('.traits-buttons').filter(function() {
          return $(this).css('display') !== 'none';}).length === 0 )
     {
        $('#traits-search-failed').show();
     }
     else{
      $('#traits-search-failed').hide();
     }
    }
  },

  click_sort: function(event) {
    //toggle showing sort options
     $('#traits-break').toggle('normal');
    $('#sort-options').toggle('normal');
  },

  click_link: function(event) {
    $('.bold').toggleClass('bold link2');
     $(event.currentTarget).toggleClass('link2 bold');
     this.sort($(event.currentTarget).text());
     

     
  },

  sort: function(string) {

      $('#traits-page-buttons').children().hide();
      if(string === 'Alphabetically')
      {
          
        $('#traits-butts').show();
       
      }
      else if(string === 'Reverse'){
        $('#traits-buttons-reverse').show();
      }
      else if(string === 'By risk'){

      }
     

     
  }
});

});


    