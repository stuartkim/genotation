$(function() {
window.GenericView = Backbone.View.extend({
  el: $('#exercise-content'),
  name: 'Bone-Mineral-Density-Kim',  
  table_id: '#disease_bmd_table',
  template_id: '#disease_bmd_template',
  url: '/media/template/lectures/disease_bmd.html',

  initialize: function() {
    _.bindAll(this, 'loaded',
      'start',
      'display',
      'finish'
    );
  },
  
  render: function() {
    $.get(this.url, this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  this.table_template = $(this.template_id).html();
  },
  
  start: function(response) {
    $.get('/media/help/disease_bmd.html', {}, function(response) {
      $('#help-exercise-help').html(response);
    });
    return true;
  },
  
  display: function(response, all_dbsnps, extended_dbsnps) {
    var self = this;
    var or_total = 0;
    var bmd_count = 0;
    var total = 0;
    
    $.each(response['snps'], function(i, v) {
      _.extend(v, extended_dbsnps[i]);
      if (v['genotype'] != '??') {
        bmd_count += count_genotype(v['genotype'], v['risk_allele']);
        total += 2;
        if (count_genotype(v['genotype'], v['risk_allele']) > 0) {
          or_total += count_genotype(v['genotype'], v['risk_allele'])*Math.log(v['combined_or']);
        }
      }
      $(self.table_id + " > tbody").append(_.template(self.table_template, v));
    });
    $(this.table_id).show();
    $(this.table_id).tablesorter();
    $('#table-options').show();
    
    this.finish(bmd_count, total, or_total);
  },
  
  finish: function(bmd_count, total, or_total) {
    $('#disease_bmd_count').html(bmd_count);
    $('#disease_bmd_total').html(total);
    $('#disease_bmd_risk').html(or_total.toFixed(4));
    $('#disease_bmd_chart').show();
  }
});
});
