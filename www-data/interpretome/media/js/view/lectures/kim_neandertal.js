$(function() {
window.GenericView = Backbone.View.extend({
  el: $('#exercise-content'),
  name: 'Neandertal-Kim',  
  table_id: '#kim_neandertal_table',
  template_id: '#kim_neandertal_template',
  url: '/media/template/lectures/kim_neandertal.html',

  initialize: function() {
    _.bindAll(this, 'loaded',
      'start',
      'display',
      'finish'
    );
  },
  
  render: function() {
    $.get(this.url, this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  this.table_template = $(this.template_id).html();
  },
  
  start: function(response) {
    $.get('/media/help/kim_neandertal.html', {}, function(response) {
      $('#help-exercise-help').html(response);
    });
    return true;
  },
  
  display: function(response, all_dbsnps, extended_dbsnps) {
    var self = this;
    var cad_count = 0;
    var total = 0;
    
    $.each(response['snps'], function(i, v) {
      _.extend(v, extended_dbsnps[i]);
      if (v['genotype'] != '??') {
        cad_count += count_genotype(v['genotype'], v['risk']);
        total += 2;
      }
      $(self.table_id + " > tbody").append(_.template(self.table_template, v));
    });
    $(this.table_id).show();
    $(this.table_id).tablesorter();
    $('#table-options').show();
    
    this.finish(cad_count, total);
  },
  
  finish: function(cad_count, total) {
    $('#kim_neandertal_count').html(cad_count);
    $('#kim_neandertal_total').html(total);
  }
});
});
