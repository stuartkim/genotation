$(function() {
window.GenericView = Backbone.View.extend({
  el: $('#exercise-content'),
  name: 'Neuro-Gitler', 
  table_id: '#gitler_neuro_table',
  template_id: '#gitler_neuro_template',
  url: '/media/template/lectures/gitler_neuro.html',

  initialize: function() {
    _.bindAll(this, 'loaded',
      'start',
      'display',
      'finish'
    );
  },
  
  render: function() {
    $.get(this.url, this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  this.table_template = $(this.template_id).html();
  },
  
  start: function(response) {
    $.get('/media/help/gitler_neuro.html', {}, function(response) {
      $('#help-exercise-help').html(response);
    });
    return true;
  },
  
  display: function(response, all_dbsnps, extended_dbsnps) {
    var self = this;
    var pd_count = 0;
    var ad_count = 0;
    var als_count = 0;
    var ftd_count = 0;
    var pd_total = 0;
    var ad_total = 0;
    var als_total = 0;
    var ftd_total = 0;
    
    $.each(response['snps'], function(i, v) {
      _.extend(v, extended_dbsnps[i]);
      //console.log(v);
      if (v['genotype'] != '??' & v['disease'] == 'PD') {
        pd_count += count_genotype(v['genotype'], v['risk_allele']);
        pd_total += 2;
      }
      if (v['genotype'] != '??' & v['disease'] == 'AD') {
        ad_count += count_genotype(v['genotype'], v['risk_allele']);
        ad_total += 2;
      }
      if (v['genotype'] != '??' & v['disease'] == 'ALS') {
        als_count += count_genotype(v['genotype'], v['risk_allele']);
        als_total += 2;
      }
      if (v['genotype'] != '??' & v['disease'] == 'FTD') {
        ftd_count += count_genotype(v['genotype'], v['risk_allele']);
        ftd_total += 2;
      }
      $(self.table_id + " > tbody").append(_.template(self.table_template, v));
    });
    $(this.table_id).show();
    $(this.table_id).tablesorter();
    $('#table-options').show();
    v={}
    v['risk_allele'] = ''
    v['disease'] = 'PD'
    v['genes'] = ''
    v['imputed_from'] = ''
    v['odds_ratio'] = ''
    v['explain'] = ''
    v['r_squared'] = ''
    v['dbsnp']='Percentage PD Risk alleles';
    v['genotype']=Math.round(pd_count/pd_total*1000)/10;
    $(self.table_id + " > tbody").append(_.template(self.table_template, v));
    
    v={}
    v['risk_allele'] = ''
    v['disease'] = 'AD'
    v['genes'] = ''
    v['imputed_from'] = ''
    v['odds_ratio'] = ''
    v['explain'] = ''
    v['r_squared'] = ''
    v['dbsnp']='Percentage AD Risk alleles';
    v['genotype']=Math.round(ad_count/ad_total*1000)/10;
    $(self.table_id + " > tbody").append(_.template(self.table_template, v));
    
    v={}
    v['risk_allele'] = ''
    v['disease'] = 'ALS'
    v['genes'] = ''
    v['imputed_from'] = ''
    v['odds_ratio'] = ''
    v['explain'] = ''
    v['r_squared'] = ''
    v['dbsnp']='Percentage ALS Risk alleles';
    v['genotype']=Math.round(als_count/als_total*1000)/10;
    $(self.table_id + " > tbody").append(_.template(self.table_template, v));
    
    v={}
    v['risk_allele'] = ''
    v['disease'] = 'FTD'
    v['genes'] = ''
    v['imputed_from'] = ''
    v['odds_ratio'] = ''
    v['explain'] = ''
    v['r_squared'] = ''
    v['dbsnp']='Percentage FTD Risk alleles';
    v['genotype']=Math.round(ftd_count/ftd_total*1000)/10;
    $(self.table_id + " > tbody").append(_.template(self.table_template, v));
    
    this.finish(pd_count, ad_count, pd_total, ad_total, als_count, als_total, ftd_count, ftd_total);
  },
  
  finish: function(pd_count, ad_count, pd_total, ad_total, als_count, als_total, ftd_count, ftd_total) {
    $('#gitler_neuro_pd').html(pd_count);
    $('#gitler_neuro_ad').html(ad_count);
    $('#gitler_neuro_als').html(als_count);
    $('#gitler_neuro_ftd').html(ftd_count);
    $('#gitler_neuro_pd_total').html(pd_total);
    $('#gitler_neuro_ad_total').html(ad_total);
    $('#gitler_neuro_als_total').html(als_total);
    $('#gitler_neuro_ftd_total').html(ftd_total);
    $('#gitler_neuro_pd_ratio').html(Math.round(pd_count/pd_total*1000)/10);
    $('#gitler_neuro_ad_ratio').html(Math.round(ad_count/ad_total*1000)/10);
    $('#gitler_neuro_als_ratio').html(Math.round(als_count/als_total*1000)/10);
    $('#gitler_neuro_ftd_ratio').html(Math.round(ftd_count/ftd_total*1000)/10);
    $('#gitler_neuro_chart').show();
  }
});
});
