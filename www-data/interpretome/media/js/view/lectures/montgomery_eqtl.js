$(function() {
window.GenericView = Backbone.View.extend({
  el: $('#exercise-content'),
  name: 'Cardiology-Ashley',  
  table_id: '#montgomery_eqtl_table',
  template_id: '#montgomery_eqtl_template',
  url: '/media/template/lectures/montgomery_eqtl.html',

  initialize: function() {
    _.bindAll(this, 'loaded',
      'start',
      'display',
      'finish'
    );
  },
  
  render: function() {
    $.get(this.url, this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  this.table_template = $(this.template_id).html();
  },
  
  start: function(response) {
    $.get('/media/help/montgomery_eqtl.html', {}, function(response) {
      $('#help-exercise-help').html(response);
    });
    return true;
  },
  
  display: function(response, all_dbsnps, extended_dbsnps) {
    var self = this;
    
    var cad_count = 0;
    var total = 0;
    $.each(response['snps'], function(i, v) {
      _.extend(v, extended_dbsnps[i]);
      if (v['genotype'] != '??') {
        cad_count += count_genotype(v['genotype'], v['risk_allele']);
        total += 2;
      }
      $(self.table_id + " > tbody").append(_.template(self.table_template, v));
    });
    $(this.table_id).show();
    $(this.table_id).tablesorter();
    $('#table-options').show();
    this.finish(cad_count, total);
  },
  
  finish: function(cad_count, total) {
    $('#montgomery_eqtl_count').html(cad_count);
    $('#montgomery_eqtl_total').html(total);
    $('#montgomery_eqtl_chart').show();
  }
});
});
