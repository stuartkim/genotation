$(function() {
window.GenericView = Backbone.View.extend({
  el: $('#exercise-content'),
  name: 'eGFR-CystatinC-Kim',  
  table_id: '#disease_egfrcys_table',
  template_id: '#disease_egfrcys_template',
  url: '/media/template/lectures/disease_egfrcys.html',

  initialize: function() {
    _.bindAll(this, 'loaded',
      'start',
      'display',
      'finish'
    );
  },
  
  render: function() {
    $.get(this.url, this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  this.table_template = $(this.template_id).html();
  },
  
  start: function(response) {
    $.get('/media/help/disease_egfrcys.html', {}, function(response) {
      $('#help-exercise-help').html(response);
    });
    return true;
  },
  
  display: function(response, all_dbsnps, extended_dbsnps) {
    var self = this;
    var or_total = 0;
    var ckd_count = 0;
    var total = 0;
    
    $.each(response['snps'], function(i, v) {
      _.extend(v, extended_dbsnps[i]);
      if (v['genotype'] != '??') {
        ckd_count += count_genotype(v['genotype'], v['risk_allele']);
        total += 2;
        if (count_genotype(v['genotype'], v['risk_allele']) > 0) {
          or_total += count_genotype(v['genotype'], v['risk_allele'])*Math.log(v['combined_or']);
        }
      }
      $(self.table_id + " > tbody").append(_.template(self.table_template, v));
    });
    $(this.table_id).show();
    $(this.table_id).tablesorter();
    $('#table-options').show();
    
    this.finish(ckd_count, total, or_total);
  },
  
  finish: function(ckd_count, total, or_total) {
    $('#disease_egfrcys_count').html(ckd_count);
    $('#disease_egfrcys_total').html(total);
    $('#disease_egfrcys_risk').html(or_total.toFixed(4));
    $('#disease_egfrcys_chart').show();
  }
});
});
