$(function() {
window.GenericView = Backbone.View.extend({
  el: $('#exercise-content'),
  name: 'Neuro-Intelligence',  
  table_id: '#neuro_intelligence_table',
  template_id: '#neuro_intelligence_template',
  url: '/media/template/lectures/neuro_intelligence.html',

  initialize: function() {
    _.bindAll(this, 'loaded',
      'start',
      'display',
      'finish'
    );
  },
  
  render: function() {
    $.get(this.url, this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  this.table_template = $(this.template_id).html();
  },
  
  start: function(response) {
    $.get('/media/help/neuro_intelligence.html', {}, function(response) {
      $('#help-exercise-help').html(response);
    });
    return true;
  },
  
  display: function(response, all_dbsnps, extended_dbsnps) {
    var self = this;
    var cad_count = 0;
    var total = 0;
    
    $.each(response['snps'], function(i, v) {
      _.extend(v, extended_dbsnps[i]);
      if (v['genotype'] != '??') {
        cad_count += count_genotype(v['genotype'], v['risk']);
        total += 2;
      }
      $(self.table_id + " > tbody").append(_.template(self.table_template, v));
    });
    $(this.table_id).show();
    $(this.table_id).tablesorter();
    $('#table-options').show();
    
    this.finish(cad_count, total);
  },
  
  finish: function(cad_count, total) {
    $('#neuro_intelligence_count').html(cad_count);
    $('#neuro_intelligence_total').html(total);
    //$('#ashley_diabetes_chart').show();
  }
});
});
