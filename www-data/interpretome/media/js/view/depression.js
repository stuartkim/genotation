//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.DepressionView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#depression'),

  buttons: {

    html: 'depression',

    amd: {
        id: 'get-gwas',
        //search term in disease_trait
        disease: 'depression',
        //type of table: OR, beta, p-value_text
        typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: '',//should the table have a running risk score column? yes/no
        //type specific or multiple or pvalue(measurement) in the quotes if needed.
        specificOrMultiple: ''

    }

  },
  
  
  events: { 
    'click button': 'click_button'
},

//sets the starting things.
  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    
//define the click button function. calls the click button function in view_functions.
  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});