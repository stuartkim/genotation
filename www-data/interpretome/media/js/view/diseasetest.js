$(function() {
window.DiseasetestView = Backbone.View.extend({
  has_loaded: false,
  el: $('#diseasetest'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/diseasetest.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#diseasetest-table').hide();
	  this.disease_template = $('#diseasetest-template').html();
	  match_style(this.el);
    $('#diseasetest-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#diseasetest-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#diseasetest-table').show();
    $('#diseasetest-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('diseasetest-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas_catalog', {population: get_user().population, disease: 'Bipolar disorder'}, this.got_diseases);
  }
  });
});
