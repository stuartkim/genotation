$(function() {
window.AutotestView = Backbone.View.extend({
  has_loaded: false,
  el: $('#autotest'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/autotest.html', this.loaded);
      $.get('/media/template/tablelegends.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#celiac-table').hide();
	  this.disease_template = $('#celiac-template').html();
	  match_style(this.el);
    $('#celiac-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#celiac-table > tbody').append(_.template(self.disease_template, v));
      }
            else {
v['genotype'] = '??';
 $('#forced-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#celiac-table').show();
    $('#celiac-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('celiac-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Celiac disease'}, this.got_diseases);
  }
  });
});
