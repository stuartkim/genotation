//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.PhytoView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#phyto'),

  buttons: {

    html: '/vitamins/phyto',

    amd: {
        id: 'get-gwas',
        //search term in disease_trait
        disease: 'phytosterol',
        //type of table: OR, beta, p-value_text
        typeOfTable: '',//type of table (null, grs, pvalue)
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        category: '',  //category used in grs table (bmd)
        //type specific or multiple or pvalue(measurement) in the quotes if needed.
        specificOrMultiple: '',
        data_table: google.visualization.arrayToDataTable([
          ['Scores', 'Population', 'Yours'],
[0.0, 330, 0 ], [1.2,   0, 0 ], [2.4,   0, 0 ], [3.6,   0, 0 ], [4.8,   0, 0 ], 
[6.0,   0, 0 ], [7.2,   0, 0 ], [8.4,   0, 0 ], [9.6,   0, 0 ], [10.8,   0, 0 ], 
[12.0, 356, 0 ], [13.2,   0, 0 ], [14.4,   0, 0 ], [15.6,   0, 0 ], [16.8,   0, 0 ], 
[18.0,   0, 0 ], [19.2,   0, 0 ], [20.4,   0, 0 ], [21.6,   0, 0 ], [22.8,  68, 0 ], 

])

    }

  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});