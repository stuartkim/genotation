$(function() {
window.ThyroidView = Backbone.View.extend({
  has_loaded: false,
  el: $('#thyroid'),

  
  
  events: { 'click #thyroid_cancer-get-gwas': 'click_thyroid_cancer'
, 'click #thyroid_cancer-get-gwas': 'click_thyroid_cancer'
},


  initialize: function() {
    _.bindAll(this, 'loaded', 'click_thyroid_cancer', 'got_thyroid_cancer'
, 'click_thyroid_cancer', 'got_thyroid_cancer'
);
  },
  
  render: function() {
    $.get('/media/template/thyroid.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#thyroid_cancer-table').hide();
	  this.disease_template = $('#thyroid_cancer-template').html();
	  match_style(this.el);
    $('#thyroid_cancer-table').tablesorter({ 
        sortList: [[TABLE_SORT_COLUMN,TABLE_SORT_ORDER]] 
    });
    this.has_loaded = true;
  }
,
  
  got_thyroid_cancer: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#thyroid_cancer-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#thyroid_cancer-table').show();
    $('#thyroid_cancer-table').trigger('update');
    $('#looking-up').dialog('close');
  }
  
,


  click_thyroid_cancer: function(event) {
    clear_table('thyroid_cancer-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'thyroid_cancer_SPACE'}, this.got_thyroid_cancer);
  }
});

});
