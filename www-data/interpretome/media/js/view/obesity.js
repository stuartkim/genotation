//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.ObesityView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#obesity'),

  buttons: {

    html: 'obesity',

    amd: {
        id: 'get-gwas',
        //search term in disease_trait
        disease: 'obesity',
        category: 'obesityceu',  //category needed if using GRS table (bmd)
        typeOfTable: 'grs',//type of table to draw (null,pvalue,grs)
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        //type specific or multiple 
        specificOrMultiple: 'grs'
//  data_table: google.visualization.arrayToDataTable([
//           
//           // [x value, y value, null]
//           ['Scores', 'Population', 'Yours'],
//           [53.253,   3, 0 ], [53.953,   2, 0 ], [54.653,   1, 0 ], [55.353,   2, 0 ], [56.053,   2, 0 ], 
//           [56.753,   4, 0 ], [57.453,  10, 0 ], [58.153,  12, 0 ], [58.853,  16, 0 ], [59.553,  16, 0 ], 
//           [60.253,  26, 0 ], [60.953,  31, 0 ], [61.653,  23, 0 ], [62.353,  27, 0 ], [63.053,  47, 0 ], 
//           [63.753,  44, 0 ], [64.453,  57, 0 ], [65.153,  54, 0 ], [65.853,  56, 0 ], [66.553,  37, 0 ], 
//           [67.253,  46, 0 ], [67.953,  38, 0 ], [68.653,  39, 0 ], [69.353,  31, 0 ], [70.053,  25, 0 ], 
//           [70.753,  20, 0 ], [71.453,  16, 0 ], [72.153,  18, 0 ], [72.853,  14, 0 ], [73.553,  17, 0 ], 
//           [74.253,   6, 0 ], [74.953,   3, 0 ], [75.653,   6, 0 ], [76.353,   0, 0 ], [77.053,   3, 0 ], 
//           [77.753,   1, 0 ], [78.453,   0, 0 ], [79.153,   1, 0 ]
//         ])
    }

  },
  
  
  events: { 
    'click button': 'click_button'
},

//sets the starting things.
  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    
//define the click button function. calls the click button function in view_functions.
  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});