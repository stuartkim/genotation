//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.QtView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#qt'),

  buttons: {

    html: 'qt',

    amd: {
        id: 'get-gwas',
        //search term in disease_trait
        disease: 'QT interval',
        category: '',  //category needed if using GRS table (bmd)
        typeOfTable: '',//type of table to draw (null,pvalue,grs)
        geneticRiskScore: '',//should the table have a running risk score column? yes/no
        //type specific or multiple 
        specificOrMultiple: ''

    }

  },
  
  
  events: { 
    'click button': 'click_button'
},

//sets the starting things.
  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    
//define the click button function. calls the click button function in view_functions.
  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});