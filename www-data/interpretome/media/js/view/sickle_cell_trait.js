$(function() {
window.SickleCellTraitView = Backbone.View.extend({
  has_loaded: false,
  el: $('#sickle_cell_trait'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/sickle_cell_trait.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#bipolar-table').hide();
	  this.disease_template = $('#bipolar-template').html();
	  match_style(this.el);
    $('#bipolar-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#bipolar-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#bipolar-table').show();
    $('#bipolar-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('bipolar-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Bipolar Disorder'}, this.got_diseases);
  }
  });
});
