//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.Type1View = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#type1'),

  buttons: {

    html: 'type1',

    amd: {
        id: 'get-gwas',
        //search term in disease_trait
        disease: 'type 1',
        //type specific or multiple in the quotes if needed.
        specificOrMultiple: ''

    }

  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});