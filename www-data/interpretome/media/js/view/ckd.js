//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.CkdView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#ckd'),

  buttons: {

    html: '/kidney/ckd',

    amd: {
        id: 'get-gwas',
        //search term in disease_trait
        disease: 'ckd',
        category: 'ckd',
        //type of table: OR, beta, p-value_text
        typeOfTable: 'grs',//type of table to draw from interpretome.html
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        //type specific or multiple or pvalue(measurement) in the quotes if needed.
        specificOrMultiple: 'grs',
        data_table: google.visualization.arrayToDataTable([
          ['Scores', 'Population', 'Yours'],
[5.286, 15, 0 ], [5.786, 0, 0 ], [6.286, 58, 0 ], [6.786, 109, 0 ], [7.286, 10, 0 ], 
[7.786, 169, 0 ], [8.286, 4, 0 ], [8.786, 88, 0 ], [9.286, 68, 0 ], [9.786, 30, 0 ], 
[10.286, 89, 0 ], [10.786, 3, 0 ], [11.286, 70, 0 ], [11.786, 0, 0 ], [12.286, 28, 0 ], 
[12.786, 0, 0 ], [13.286, 10, 0 ], [13.786, 0, 0 ], [14.286, 2, 0 ], [14.786, 1, 0 ], 

])

    }

  },
  
  
  events: { 
    'click button': 'click_button'
},

//sets the starting things.
  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    
//define the click button function. calls the click button function in view_functions.
  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});