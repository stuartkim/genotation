$(function() {
window.GliomaView = Backbone.View.extend({
  has_loaded: false,
  el: $('#glioma'),

  
  
  events: { 'click #get-gwas': 'click_disease'},


  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/glioma.html', this.loaded);
  },
    
    loaded: function(response) {
    $(this.el).append(response);
    $('button').button();
    $('#table').append(getTableModel());
    $('.results-table').hide();
    this.disease_template = $('.results-template').html();
    match_style(this.el);

    this.has_loaded = true;
    
  },
  
  got_diseases: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('.results-table > tbody').append(_.template(self.disease_template, v));
      }
      else {

      }
    });
    $('.results-table').show();
    $('.results-table').trigger('update');
    $('.results-table').tablesorter();
    $('#looking-up').dialog('close');
  },


  click_disease: function(event) {
    clear_table('results-table');
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Osteoarthritis'}, this.got_diseases);
  }
  });

});
