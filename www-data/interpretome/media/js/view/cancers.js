var searchTermCancers;

$(function() {
window.CancersView = Backbone.View.extend({
  has_loaded: false,
  el: $('#cancers'),

  
  
  events: {
  'keyup': 'change_search_yeah',
  'click #cancer-sort': 'click_sort',
  'click .link2': 'click_link'
},


  initialize: function() {
    _.bindAll(this, 'loaded', 'change_search_yeah', 'click_sort');
  },
  
  render: function() {
    $.get('/media/template/cancers.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $("#cancer-sort").hover(function(){
    $(this).css("background-color", "#e0e0e0");
    $(this).css("border-bottom-width", "2px");
    $(this).css("padding-bottom", "3px");
    $(this).css("cursor", "pointer");
    }, function(){
    $(this).css("background-color", "white");
    $(this).css("border-bottom-width", "1px");
    $(this).css("padding-bottom", "4px");
});

  
	  match_style(this.el);
    this.has_loaded = true;
  }
,


  change_search_yeah: function(event) {

    searchTermCancers = $('#cancer-search').val(); 
     for(var i =0; i < cancersList.length; i++)
     {
        if(cancersList[i].toLowerCase().indexOf(searchTermCancers.toLowerCase())===-1){
            $("#cancer-"+cancersList[i]).hide();
        }
        if(cancersList[i].toLowerCase().indexOf(searchTermCancers.toLowerCase())>-1)
        {
          $("#cancer-"+cancersList[i]).show();
        }
     }
    for(var i =0; i < nicknameList.length; i++)
     {
        if(nicknameList[i].toLowerCase().indexOf(searchTermCancers.toLowerCase())===-1){
            $("#cancer-"+hasNicknameList[i]).hide();
        }
        if(nicknameList[i].toLowerCase().indexOf(searchTermCancers.toLowerCase())>-1)
        {
          $("#cancer-"+hasNicknameList[i]).show();
        }
     }
     if( $('#cancer-butts').children().filter(function() {
  return $(this).css('display') !== 'none';}).length === 0 )
     {
        $('#cancer-search-failed').show();
     }
     else{
      $('#cancer-search-failed').hide();
     }
    
  },

  click_sort: function(event) {
    
     $('#cancer-break').toggle('normal');
    $('#cancer-sort-options').toggle('normal');
  },

  click_link: function(event) {

    $('.bold').toggleClass('bold link2');
     $(event.currentTarget).toggleClass('link2 bold');
     this.sort($(event.currentTarget).text());
     

     
  },

  sort: function(string) {

      $('#cancer-page-buttons').children().hide();
      if(string === 'Alphabetically')
      {
          
        $('#cancer-butts').show();
       
      }
      else if(string === 'Reverse'){
        $('#cancer-buttons-reverse').show();
      }
      else if(string === 'By risk'){

      }
     

     
  }
});

});


    