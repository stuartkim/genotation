$(function() {
//replace ASthma with new table name.
window.AmdView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#amd'),

  buttons: {

    html: 'amd',

    amd: {
        id: 'get-gwas',
        //search term in disease_trait
        disease: 'Age-related macular degeneration',
        //type of table: OR, beta, p-value_text
           typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: 'no',//should the table have a running risk score column? yes/no
        //type specific or multiple or pvalue(measurement) in the quotes if needed.
        specificOrMultiple: ''

    }

  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});

