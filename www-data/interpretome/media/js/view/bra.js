//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.BraView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#bra'),

  buttons: {

    html: 'bra',

    amd: {
        id: 'get-gwas',
        //search term in disease_trait
        disease: 'breast size',
        typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        //type specific or multiple in the quotes if needed.
       
        specificOrMultiple: '',
        data_table: google.visualization.arrayToDataTable([
          ['Scores', 'Population', 'Yours'],
[-0.1656,  18, 0 ], [-0.1166,   0, 0 ], [-0.0676,   0, 0 ], [-0.0186, 180, 0 ], [0.0304,   0, 0 ], 
[0.0794,   0, 0 ], [0.1284, 424, 0 ], [0.1774,   0, 0 ], [0.2264,   0, 0 ], [0.2754,   0, 0 ], 
[0.3244,  40, 0 ], [0.3734,   0, 0 ], [0.4224,   0, 0 ], [0.4714,  85, 0 ], [0.5204,   0, 0 ], 
[0.5694,   0, 0 ], [0.6184,   0, 0 ], [0.6674,   0, 0 ], [0.7164,   0, 0 ], [0.7654,   0, 0 ], 
[0.8144,   7, 0 ], 
]),

    }

  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});