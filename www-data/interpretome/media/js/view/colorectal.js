$(function() {
window.Colorectalview = Backbone.View.extend({
  has_loaded: false,
  el: $('#colorectal'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/colorectal.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#colorectal-table').hide();
	  this.disease_template = $('#colorectal-template').html();
	  match_style(this.el);
    $('#colorectal-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#colorectal-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#colorectal-table').show();
    $('#colorectal-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('colorectal-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Bipolar  disorder'}, this.got_diseases);
  }
  });
});
