$(function() {
window.TablelegendsView = Backbone.View.extend({
  has_loaded: false,
  el: $('#tablelegends'),

  
  initialize: function() {
    _.bindAll(this, 'loaded');
  },
  
  render: function() {
    $.get('/media/template/tablelegends.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  
	  match_style(this.el);
  
    this.has_loaded = true;
  },
  
  
  });
});
