$(function() {
window.SickleView = Backbone.View.extend({
  has_loaded: false,
  el: $('#sickle'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/sickle.html', this.loaded);
      $.get('/media/template/tablelegends.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#sickle-table').hide();
	  this.disease_template = $('#sickle-template').html();
	  match_style(this.el);
    $('#sickle-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
  
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(v['strongest_snp']);
         if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#sickle-table > tbody').append(_.template(self.disease_template, v));
      }
            else {
v['genotype'] = '??';
 $('#sickle-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#sickle-table').show();
    $('#sickle-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('sickle-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'sickle cell trait'}, this.got_diseases);
  }
  });
});
