//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.ForcedView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#forced'),

  buttons: {

    html: 'forced',

    button1: {
        id: 'get-gwas-1',
        //search term in disease_trait
         typeOfTable: '',//type of table (null, grs, pvalue)
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        category: '',  //category used in grs table (bmd)
        disease: 'forced',
        //mysql search type (null,specific, multiple)
        specificOrMultiple: ''
        
        

    },
    //button 2
    button2: {
        id: 'get-gwas-2',
        //search term in disease_trait
        disease: 'pulmonary function',
        typeOfTable: 'pvalue',//type of table to draw from interpretome.html
        geneticRiskScore: '',//should the table have a running risk score column? yes/no
        category: '',
        specificOrMultiple: ''
},


  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});