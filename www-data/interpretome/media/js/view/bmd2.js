$(function() {
window.Bmd2View = Backbone.View.extend({
  has_loaded: false,
  el: $('#bmd2'),  //looks up bmd2 url.

  events: { 'click #get-gwas': 'click_disease' },//when click #get_gwas, then do click_disease.

  //initializes functions defined below
  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'draw_chart', 'got_impute', 'loaded');
  },// _.bindAll mean-> when call method of that name, use method in this js file.  "this"-> top level object
  
  render: function() {
    $.get('/media/template/bmd2.html', this.loaded);
    $.get('/media/template/tablelegends.html', this.loaded);
  },
    
  //loaded initializes the functions
  loaded: function(response) {
    $(this.el).append(response);
    $('button').button();
    $('#bmd2-table').hide();
    this.disease_template = $('#bmd2-template').html();
    match_style(this.el);
    $('#bmd2-table').tablesorter();
    this.has_loaded = true;
    this.running_total = 0;  // makes it accessible in both got_diseases and got_impute
  },

  //got_diseases looks up genotype or imputes 
  got_diseases: function(response) {
    var self = this;
    user = get_user();
    self.running_total = 0;

    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['dbsnp']));
      if (dbsnp != undefined && dbsnp.genotype != "--"){ // if the user's genotype is defined at this snp
        v['genotype'] = dbsnp.genotype;  
        if(v['risk_allele'].charAt(0) !== "?") { 
          self.running_total += calculate_lr(v['risk_allele'], v['genotype'], v['beta']);   
        }
        $('#bmd2-table > tbody').append(_.template(self.disease_template, v));
        $('#bmd2-table tr:last').append('<td>' + self.running_total.toFixed(2) + '</td>');   // adds running LR column
      } else {  // imputes using impute_table in views.py and got_impute below
       $.get('/bmd2/impute_table/', {strongest_snp: v['dbsnp'], dbsnp: v['dbsnp'], genotype: v['genotype'], risk_allele: v['risk_allele'], beta: v['beta']}, self.got_impute); 
      }
    });
    $('#bmd2-table').show();
    $('#bmd2-table').trigger('update');
    $('#looking-up').dialog('close');

    $.get('/bmd2/', {population: get_user().population}, this.draw_chart); 
  },
  
  //imputes or uses population average. does not do the phase thing yet.
  got_impute: function(response) {
    if (response != null) {
      response['dbsnp'] = response['strongest_snp'] + "*";
      response['genotype'] = "";
      user = get_user();
      var anchor_genotype = user.lookup(filter_identifier(response['anchor'])); // returns an object ex. {genotype: "CT"}

      if (anchor_genotype != undefined && response['phase'] != null && response['phase'] != 0 && response['refallele'] != null && response['otherallele'] != null) {
        if (response['phase'] == 1) { // if it is phase 1
          for (var i = 0; i < 2; i++) {
            if (anchor_genotype.genotype.charAt(i) == response['anchor_ref']) {
              response['genotype'] += response['refallele'].trim();
            } else {
              response['genotype'] += response['otherallele'].trim();
            }
          }
        } else {
          for (var i = 0; i < 2; i++) {
            if (anchor_genotype.genotype.charAt(i) == response['anchor_ref']) {
              response['genotype'] += response['otherallele'].trim();
            } else {
              response['genotype'] += response['refallele'].trim();
            }
          }
        }
        if (response['risk_allele'] != "?" && !isNaN(response['or_or_beta'])) {
          this.running_total += calculate_lr(response['risk_allele'], response['genotype'], response['or_or_beta']); 
        }   
      } else if (response['freq'] != undefined) {  // if the anchor snp/genotype doesn't exist  
        response['genotype'] = "??"
        if (response['risk_allele'] != "?" && !isNaN(response['or_or_beta'])) {
          this.running_total += 2 * response['freq'] * response['or_or_beta'];
        }
      }
    }

    $('#bmd2-table > tbody').append(_.template(this.disease_template, response));
    $('#bmd2-table tr:last').append('<td>' + this.running_total.toFixed(2) + '</td>');
  },

//draw histogram. 
  
  draw_chart: function(response) {
    data = new google.visualization.DataTable();  
    data.addColumn('number', 'Scores')
    data.addColumn('number', 'Population Scores');
    data.addColumn('number', 'Your Score');
    //[x,y,place holder for your score]
    data.addRows([
    [50.942,   0, null ], [53.242,   0, null ], [56.942,   1, null ], [57.642,   0, null ], [58.342,   1, null ], [59.042,   3, null ], [59.742,   6, null ], 
      [60.442,  11, null ], [61.142,  18, null ], [61.842,  34, null ], [62.542,  54, null ], [63.242,  84, null ], 
      [63.942,  83, null ], [64.642, 100, null ], [65.342, 102, null ], [66.042,  95, null ], [66.742,  63, null ], 
      [67.442,  47, null ], [68.142,  33, null ], [68.842,   6, null ], [69.542,   7, null ], [70.242,   2, null ], 
      [70.942,   3, null ], [71.642,   1, null ], ]);
  var x = data.getNumberOfRows() - 1;
  for (var i = 0; i < data.getNumberOfRows() - 1; i++) {
      if (this.running_total >= data.getValue(i, 0) && this.running_total < data.getValue(i+1, 0)) {
        x = i;
      
      }
    }
    data.addRow([data.getValue(x, 0), data.getValue(x, 1), 10]);

    var chart = new google.visualization.ColumnChart(     
      document.getElementById('BMD-chart')
    );

    chart.draw(data, {
      width: 800,
      height: 400,
      title: 'BMD Scores',
      bar: { groupWidth: "95%" },
      legend: { position: 'right' },
      isStacked: true,
      tooltip: { trigger: 'none' },
    });
    $('#BMD-chart').show();

  },

//runs the show snps button
  click_disease: function(event) {
    clear_table('bmd2-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/bmd2/', {population: get_user().population, category: 'bmd'}, this.got_diseases);
  }

  });
});


