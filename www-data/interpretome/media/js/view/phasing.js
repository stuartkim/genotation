$(function() {
window.PhasingView = Backbone.View.extend({
  el: $('#phasing'),
  name:'Phasing',
  has_loaded: false,
  template_id: '#phasing_template',
  url: '/media/template/phasing.html',
  
  events: {
    'click #compute-phasing': 'click_compute_phasing'
  },
  
  initialize: function() {
    _.bindAll(this,
      'loaded',
      'update_counter'
    );
  },
  
  render: function() {
    $.get('/media/template/phasing.html', this.loaded);
  },
  
  loaded: function(response) {
    $(this.el).append(response);
    $('button').button();
    $('#phasing-genome').dialog({modal: true, resizable: false, autoOpen: false});
    $('#phasing-bar').progressbar();
    $('#download-phase').button({disabled:true});
    this.has_loaded = true;
  },
  
  click_compute_phasing: function(event) {
    if (window.App.check_genome() == false) return;
    this.update_counter(0);
    $('#phasing-genome').dialog('open');
    $.get('/get_phasing_params/', { //this isn't executing
      population: user.population
    }, this.got_phasing_params);
  },
  
  got_phasing_params: function(response) {
    console.log(response);
  },
  
  update_counter: function(chrom) {
    $('#phasing-bar').progressbar('option', 'value', 100*chrom/22);
    if (chrom == 0) {      
      document.getElementById('chrom-phased').innerHTML = 'Downloading';
    } else {
      document.getElementById('chrom-phased').innerHTML = 'Chromosome ' + chrom;      
    }
    $('#phasing-bar > div').css('opacity', 100*chrom/22);
  },
  
});
});