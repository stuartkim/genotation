$(function() {
window.VitaminView = Backbone.View.extend({
  has_loaded: false,
  el: $('#vitamin'),

  
  initialize: function() {
    _.bindAll(this, 'loaded');
  },
  
  render: function() {
    $.get('/media/template/vitamin.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  
	  match_style(this.el);
  
    this.has_loaded = true;
  },
  
  
  });
});
