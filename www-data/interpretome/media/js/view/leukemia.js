$(function() {
window.LeukemiaView = Backbone.View.extend({
  has_loaded: false,
  el: $('#leukemia'),

  
  
  events: { 'click #leukemia-get-gwas': 'click_leukemia'
, 'click #leukemia-get-gwas': 'click_leukemia'
},


  initialize: function() {
    _.bindAll(this, 'loaded', 'click_leukemia', 'got_leukemia'
, 'click_leukemia', 'got_leukemia'
);
  },
  
  render: function() {
    $.get('/media/template/leukemia.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#leukemia-table').hide();
	  this.disease_template = $('#leukemia-template').html();
	  match_style(this.el);
    $('#leukemia-table').tablesorter({ 
        sortList: [[TABLE_SORT_COLUMN,TABLE_SORT_ORDER]] 
    });
    this.has_loaded = true;
  }
,
  
  got_leukemia: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#leukemia-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#leukemia-table').show();
    $('#leukemia-table').trigger('update');
    $('#looking-up').dialog('close');
  }
  
,


  click_leukemia: function(event) {
    clear_table('leukemia-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'leukemia_SPACE'}, this.got_leukemia);
  }
});

});
