//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.TraceView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#trace'),

  buttons: {

    html: '/vitamins/trace',

    amd1: {
        id: 'get-gwas-1',
        //search term in disease_trait
        disease: 'zn levels',
        //type of table: OR, beta, p-value_text
        
        //type specific or multiple or pvalue(measurement) in the quotes if needed.
        typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        specificOrMultiple: '',//type of mysql search
data_table: google.visualization.arrayToDataTable([
          ['Scores', 'Population', 'Yours'],
[-0.15, 0, 0 ],[-0.1, 0, 0 ],[-0.0899, 268, 0 ], [-0.0719,   0, 0 ], [-0.0539,   0, 0 ], [-0.0359,   0, 0 ], [-0.0179,   0, 0 ], 
[0.0001,   0, 0 ], [0.0181,   0, 0 ], [0.0361,   0, 0 ], [0.0541,   0, 0 ], [0.0721, 367, 0 ], 
[0.0901,   0, 0 ], [0.1081,   0, 0 ], [0.1261,   0, 0 ], [0.1441,   0, 0 ], [0.1621,   0, 0 ], 
[0.1801,   0, 0 ], [0.1981,   0, 0 ], [0.2161,   0, 0 ], [0.2341,   0, 0 ], [0.2521, 119, 0 ], 

])
    },
     html: '/vitamins/trace',
       amd2: {
        id: 'get-gwas-2',
        //search term in disease_trait
        disease: '(se levels)',
        //type of table: OR, beta, p-value_text
           typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: 'no',//should the table have a running risk score column? yes/no
        //type specific or multiple or pvalue(measurement) in the quotes if needed.
        specificOrMultiple: ''

    },
       html: '/vitamins/trace',
       amd3: {
        id: 'get-gwas-3',
        //search term in disease_trait
        disease: 'cu levels',
        //type of table: OR, beta, p-value_text
           typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: 'no',//should the table have a running risk score column? yes/no
        //type specific or multiple or pvalue(measurement) in the quotes if needed.
        specificOrMultiple: ''

    },

  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});