//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.RheumatoidView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#rheumatoid'),

  buttons: {

    html: 'rheumatoid',

    amd: {
        id: 'get-gwas',
        //search term in disease_trait
        disease: 'rheumatoid arthritis',
         category: '',  //category needed if using GRS table (bmd)
        typeOfTable: '',//type of table to draw (null,pvalue,grs)
        geneticRiskScore: '',//should the table have a running risk score column? yes/no
        //type specific or multiple in the quotes if needed.
        specificOrMultiple: 'specific'

    }

  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});