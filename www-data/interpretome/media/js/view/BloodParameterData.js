//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.BloodParameterDataView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#BloodParameterData'),

  buttons: {

    html: '/blood parameters/BloodParameterData',

    button1: {
        id: 'get-gwas-1',
        //search term in disease_trait
         typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        disease: 'Red blood cell count',
        //type specific or multiple in the quotes if needed.
        specificOrMultiple: '',
        data_table: google.visualization.arrayToDataTable([
          ['Scores', 'Population', 'Yours'],
[-0.68402,   1, 0 ], [-0.65152,   3, 0 ], [-0.61902,   3, 0 ], [-0.58652,   8, 0 ], [-0.55402,   0, 0 ], 
[-0.52152,  27, 0 ], [-0.48902,   9, 0 ], [-0.45652,  19, 0 ], [-0.42402,  56, 0 ], [-0.39152,   0, 0 ], 
[-0.35902,   3, 0 ], [-0.32652,  99, 0 ], [-0.29402,  59, 0 ], [-0.26152,  27, 0 ], [-0.22902, 141, 0 ], 
[-0.19652,   4, 0 ], [-0.16402, 116, 0 ], [-0.13152,  77, 0 ], [-0.09902,   0, 0 ], [-0.06652,   0, 0 ], 
[-0.03402, 102, 0 ], 
])
        

    },
    //button 2
    button2: {
        id: 'get-gwas-2',
        //search term in disease_trait
        disease: 'Hemoglobin',
         typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: 'no',//should the table have a running risk score column? yes/no
        //type specific or multiple in the quotes if needed.
        specificOrMultiple: 'specific'
},

//button 3
    button3: {
        id: 'get-gwas-3',
        //search term in disease_trait
        disease: 'hematocrit',
         typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        //type specific or multiple in the quotes if needed.
        specificOrMultiple: '',
data_table: google.visualization.arrayToDataTable([
          ['Scores', 'Population', 'Yours'],
[-0.34016,  71, 0 ], [-0.28766,   0, 0 ], [-0.23516,   0, 0 ], [-0.18266, 225, 0 ], [-0.13016,   0, 0 ], 
[-0.07766,   5, 0 ], [-0.02516, 111, 0 ], [0.02734, 151, 0 ], [0.07984,   0, 0 ], [0.13234,  30, 0 ], 
[0.18484,  94, 0 ], [0.23734,   0, 0 ], [0.28984,  32, 0 ], [0.34234,   0, 0 ], [0.39484,  17, 0 ], 
[0.44734,   0, 0 ], [0.49984,  16, 0 ], [0.55234,   0, 0 ], [0.60484,   1, 0 ], [0.65734,   0, 0 ], 

]),
},

//button 4
    button4: {
        id: 'get-gwas-4',
        //search term in disease_trait
        disease: 'mean corpuscular hemoglobin',
         typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: 'no',//should the table have a running risk score column? yes/no
        //type specific or multiple in the quotes if needed.
        specificOrMultiple: '',

},



//button 5
    button5: {
        id: 'get-gwas-5',
        //search term in disease_trait
        disease: 'mean corpuscular volume',
         typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: 'no',//should the table haveß a running risk score column? yes/no
        //type specific or multiple in the quotes if needed.
        specificOrMultiple: ''
},

//button 6
    button6: {
        id: 'get-gwas-6',
        //search term in disease_trait
        disease: 'hematolog,erythrocyt phenotypes',
         typeOfTable: 'pvalue',//type of table (pvalue,grs or null)
        geneticRiskScore: 'no',//should the table have a running risk score column? yes/no
        //type specific or multiple in the quotes if needed.
        specificOrMultiple: 'multiple'
}
  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});