$(function() {
window.ReginaView = Backbone.View.extend({
  has_loaded: false,
  el: $('#regina'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/regina.html', this.loaded);
  },
    
  loaded: function(response) {
    $(this.el).append(response);
    $('button').button();
    $('#regina-table').hide();
    this.disease_template = $('#regina-template').html();
    match_style(this.el);
    $('#regina-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    user = get_user();

    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#regina-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#regina-table').show();
    $('#regina-table').trigger('update');
    $('#looking-up').dialog('close');



    data = new google.visualization.DataTable();  
    data.addColumn('number', 'Scores')
    data.addColumn('number', 'Population Average');
    data.addColumn('number', 'Your Score');
    data.addRows([
      [ 56.9419046, 1, null],
      [ 57.7419046, 0, null],
          [ 58.5419046, 2, null],
          [ 59.3419046, 4, null],
          [ 60.1419046, 9, null],
          [ 60.9419046, 20, null],
          [ 61.7419046, 38, null],
          [ 62.5419046, 66, null],
          [ 63.3419046, 93, null],
          [ 64.1419046, 105, null],
          [ 64.9419046, 123, null],
          [ 65.7419046, 106, null], 
          [ 66.5419046, 78, null],
          [ 67.3419046, 57, null],
          [ 68.1419046, 33, null],
          [ 68.9419046, 10, null],
          [ 69.7419046, 4, null],
          [ 70.5419046, 4, null],
          [ 71.3419046, 0, null],
          [ 72.1419046, 1, null]
      ]);
    var test_num = 65;
    var x = data.getNumberOfRows() - 1;
    console.log(x);
    data.addRow([ 64.9419046, 123, 5]);

    for (var i = 0; i < data.getNumberOfRows() - 1; i++) {
      if (test_num >= data.getValue(i, 0) && test_num < data.getValue(i+1, 0)) {
        x = i;
        console.log(data.getValue(i,0));
      }
    }
    console.log(data.getValue(x, 1));
    
    var chart = new google.visualization.ColumnChart(     
      document.getElementById('regina-chart')
    );
    
    chart.draw(data, {
      width: 800,
      height: 400,
      title: 'BMD Scores',
      bar: { groupWidth: "95%" },
      legend: { position: 'right' },
      isStacked: true,
      tooltip: { trigger: 'none' },
    });
    $('#regina-chart').show();

  },
  
  click_disease: function(event) {
    clear_table('regina-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Age-related macular degeneration'}, this.got_diseases);
  }
  });
});
