$(function() {
window.FabianView = Backbone.View.extend({
  has_loaded: false,
  el: $('#fabian'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/fabian.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#fabian-table').hide();
	  this.disease_template = $('#fabian-template').html();
	  match_style(this.el);
    $('#fabian-table').tablesorter();
    this.has_loaded = true;
  },
  
  
  
  got_diseases: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#fabian-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#fabian-table').show();
    $('#fabian-table').trigger('update');
    $('#looking-up').dialog('close');
    
    /*THIS WAS ADDED FROM GOT DIABETES SNPS*/
    get_user().lookup_snps(
      this.show_fabian_snps, response['snps'], response['dbsnps'], response
    );
  },
  
  
  
  
  
  
  
  
  
  
  
  
  
  /*
  click_compute_diabetes: function(event) {
    $('.required').hide();
    if (window.App.check_all() == false) return;
    $('#diabetes-chart').empty();
    
    $('#diabetes-table tr').slice(1).remove();
    $('#diabetes-table').hide();
    
    if (!(get_user().population in this.priors)) {
      $('#pop-error-box').empty();
      $('#please-choose-another-population').show('slow');
      $('#pop-error-box').append(get_user().population);
      return;
    }
    
    $.get('/diabetes/', {population: get_user().population}, this.got_diabetes_snps);
  },*/
  
  /*
  got_diabetes_snps: function(response) {
    get_user().lookup_snps(
      this.show_diabetes_snps, response['snps'], response['dbsnps'], response
    );
  },*/
  
  show_bipolar_snps: function(response, all_dbsnps, extended_dbsnps) {
    all_dbsnps = _.uniq(all_dbsnps);
    var self = this;
    var lr = compute_odds(this.priors[get_user().population]);
    
    data = new google.visualization.DataTable();

    /*data.addColumn('string', 'SNP');
    data.addColumn('number', 'Running LR');
    data.addColumn('number', 'Prior');
    data.addRow(['Prior', Math.round(compute_probability(lr)*1000)/10, 100 * this.priors[get_user().population]]);
*/
    data.addRow(['Prior', 10]);
    
    $('#fabian-table').append(
      '<tr><td><strong>Prior</td><td></td><td></td><td></td><td></td></tr>'
    );
    $('#fabian-table tr:last').
          append('<td>' + parseFloat(lr).toFixed(3) + '</td><td>' + parseFloat(lr).toFixed(3) +
                 '</td><td>' + parseFloat(100*compute_probability(lr)).toFixed(3) + '% </td>');
    /*
    for (var i = 0; all_dbsnps[i]; i++) {
      var user_snp = extended_dbsnps[all_dbsnps[i]];
      for (var j = 0; response[all_dbsnps[i]][j]; j++) {
        var study_snp = response[all_dbsnps[i]][j];
        if (user_snp.genotype == 'NA') continue;
        if (!compare_arrays(user_snp.genotype.split(''), study_snp.genotype.split(''))) continue;
        user_snp.study_size = study_snp.study_size;
        user_snp.LR = study_snp.LR;
        $('#diabetes-table').append(_.template(self.diabetes_snp_template, user_snp));
	      lr = lr * study_snp.LR;
	      $('#diabetes-table tr:last').
	        append('<td>' + study_snp.LR.toFixed(3) + '</td><td>' + parseFloat(lr).toFixed(3) + 
	          '</td><td>' + parseFloat(100 * compute_probability(lr)).toFixed(3) + '% </td>');
	      data.addRow([study_snp.dbsnp + '', Math.round(compute_probability(lr)*1000)/10, 100 * this.priors[get_user().population]]);
	      break;
      }*/
    }
    $('#fabian-table').show();
    $("#fabian-table").tablesorter();
  
  
  
  
  
  
  
  
  
  
  
  
  click_disease: function(event) {
    clear_table('fabian-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Fabian'}, this.got_diseases);
  }
  });
});
