$(function() {
window.NeandertalView = Backbone.View.extend({
  el: $('#neandertal'),
  name: 'Neandertal similarity', 
  table_id: '#neandertal_table',
  template_id: '#neandertal_template',
  url: '/media/template/neandertal.html',
  has_loaded: false,

//  events for buttons?
  events: {
    'click #predict_neandertal': 'click_neandertal_snps',
    'click #submit-neandertal': 'click_submit',
    'click #confirm-submit-snps': 'click_confirm_submit'
  },
  
  initialize: function() {
    _.bindAll(this, 'loaded',
      'click_neandertal_snps',
      'click_submit',
      'click_confirm_submit',
      'display',
      'finish'
    );
    //console.log('initialized');
  },
  
  render: function() {
    $.get(this.url, this.loaded);
    //console.log('rendered')
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
//	  Added this
	  // Widget initialization.
	  //$('button').button();
	  $('.help-button').button({
      icons: {primary: 'ui-icon-help'}	    
	  });
	  $('.help > div').show();
	  $('#predict_neandertal').button();
	  $('#submit-neandertal').button({disabled:true});
	  //$('#submit-neandertal').button();
	  this.table_template = $(this.template_id).html();
	  //console.log(this)
//	  Added this... need a template to show table?	  
	  this.neandertal_template = $('#neandertal-template').html();
	  this.has_loaded = true;
  },
  

  
//  Added this... makes window w/ snps appear
  click_neandertal_snps: function(response) {
    if (window.App.check_all() == false) return;
    
    var dbsnps = filter_identifiers(
      $('#neandertal-snps-textarea').val().split('\n')
    );
    var self=this;
    $.get(
      '/neandertal/get_neandertal_snps/', {
        population: user.population
      }, function(response) {
        return user.lookup_snps(
          self.display, response, _.keys(response), {}
        );
      }
    );
    //return get_user().lookup_snps(this.display, {}, dbsnps, {});
  },
  
  click_submit: function(event) {
    var self = this;
    $('#confirm-submit-exercise').dialog({
      modal: true, resizable: false, buttons: {
        'Confirm' : function() {
	  //console.log('blah');
          self.click_confirm_submit();
          $(this).dialog('close');
        },
        'Cancel': function() {$(this).dialog('close');}
      }
    });
  },
  
  click_confirm_submit: function(event) {
    var output = {};
    var self = this;
    output['total_neandertal_index']=$('#total_neandertal_index').html();
    output['total_neandertal']=$('#total_neandertal').html();
    output['exercise']='neandertal';
    output['population']=get_user().population;
    output['neandertal_23andMe']=$('#neandertal_23andMe_box').val();
    //console.log(output)
//    $('#neandertal_table td').each( function() {
//	output.push(this.innerText.split(' ')[0])
//      }
//    );
//    console.log(this.table_id)
//    data={neandertal:output.join()}
//    console.log(data)
    $.get( 'submit', output, check_submission);
  },
  
  display: function(response, all_dbsnps, extended_dbsnps) {
    var self = this;
    var total_index = 0;
    var total = 0;
    //console.log(extended_dbsnps);
    //console.log(response);
    //console.log('blah');
    console.log(response)
    $.each(response, function(i, v) {
      var snp = extended_dbsnps[i];
      if (snp != undefined){
        var count = count_genotype(snp.genotype, v['out_of_africa_allele']);
        total_index += count;
        v['count'] = count;
        v['genotype'] = snp.genotype;
        $(self.table_id + " > tbody").append(_.template(self.table_template, v));
        total += 2;
      }
    });
    $('#count_neandertal').show()
    //console.log(v)
    $('#show_pics').show()
    $(this.table_id).show();
    $(this.table_id).tablesorter();
    $('#table-options').show();
    $('#neandertal_23andMe').show();
    $("#submit-neandertal").button("option", "disabled", false);
    //$("#submit-neandertal").button().show();
    
    this.finish(total_index, total);
  },
  
  finish: function(total_index, total) {
    data = new google.visualization.DataTable();
    $('#total_neandertal_index').html(total_index);
    $('#total_neandertal').html(total);
    data.addColumn('string', 'Label');
    data.addColumn('number', 'Value');
    data.addRows(1);
    data.setValue(0, 0, 'Neandertal');
    data.setValue(0, 1, total_index);
    this.fb_text = "My Neandertal index is " + total_index + " what's yours?"; 
    var chart = new google.visualization.Gauge(document.getElementById('neandertal_chart'));
    var options = {width: 900, height: 300, redFrom: total/4, redTo: 84,
        min: 0, max: total,
        yellowFrom:total/8, yellowTo: total/4, minorTicks: 5};
    chart.draw(data, options);
    
    $('#neandertal_chart').show();
  }
});
});
