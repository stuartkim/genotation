
//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.CeliacView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#celiac'),

  buttons: {

    html: 'celiac',

    amd1: {
        id: 'get-gwas1',
        //search term in disease_trait
        disease: 'Celiac disease',
        category: 'celiac',
        //type specific or multiple in the quotes if needed.
         typeOfTable: 'grs',//type of table to draw from interpretome.html
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        specificOrMultiple: 'grs',
        
        data_table: google.visualization.arrayToDataTable([
          ['Scores', 'Population', 'Yours'],
[-0.148516024, 18, 0 ], [-0.094855674, 73, 0 ], [-0.041195324, 121, 0 ], [0.012465026, 171, 0 ], [0.066125376, 100, 0 ], 
[0.119785726, 61, 0 ], [0.173446076, 29, 0 ], [0.227106426, 19, 0 ], [0.280766776, 11, 0 ], [0.334427126, 21, 0 ], 
[0.388087476, 41, 0 ], [0.441747826, 34, 0 ], [0.495408176, 26, 0 ], [0.549068526, 11, 0 ], [0.602728876, 3, 0 ], 
[0.656389226, 6, 0 ], [0.710049576, 1, 0 ], [0.763709926, 1, 0 ], [0.817370276, 4, 0 ], [0.871030626, 2, 0 ], 
[0.924690976, 1, 0 ], 
])

    },
     amd2: {
        id: 'get-gwas2',
        //search term in disease_trait
        disease: 'celiac',
        
       typeOfTable: '',//type of table to draw from interpretome.html
        geneticRiskScore: '',//should the table have a running risk score column? yes/no//type specific, multiple or grs in the quotes if needed.
        specificOrMultiple: ''

    }


  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});
