$(function() {
window.BmdView = Backbone.View.extend({
  has_loaded: false,
  el: $('#bmd'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/bmd.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#bmd-table').hide();
	  this.disease_template = $('#bmd-template').html();
	  match_style(this.el);
    $('#bmd-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    var runningTotal = 0;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#bmd-table > tbody').append(_.template(self.disease_template, v));

        // CALCULATES LR
        if(v['risk_allele'].charAt(0) !== "?") { 
          var match = 0;
          for (var i = 0; i < v['genotype'].length; i++) {
            if (v['risk_allele'].charAt(0) === v['genotype'].charAt(i)) {
              match++;
            }
          }
          runningTotal += parseFloat(v['or_or_beta']) * match;
        }

        $('#bmd-table tr:last').append('<td>' + runningTotal.toFixed(2) + '</td>');
      }
    });
    $('#bmd-table').show();
    $('#bmd-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('bmd-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Bone mineral density'}, this.got_diseases);
  }

  });
});





  /*
  $(function() {
window.BMDView = Backbone.View.extend({
  has_loaded: false,
  el: $('#bmd'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/bmd.html', this.loaded);
  },
    
  loaded: function(response) {
    $(this.el).append(response);
    $('button').button();
    $('#bmd-table').hide();
    this.disease_template = $('#bmd-template').html();
    match_style(this.el);
    $('#bmd-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#bmd-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#bmd-table').show();
    $('#bmd-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('bmd-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Bone mineral density'}, this.got_diseases);
  }
  });
});
*/