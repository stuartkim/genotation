//This js file uses view_functions.js to fill out tables.
$(function() {
//replace ASthma with new table name.
window.BpView = Backbone.View.extend({ 
  has_loaded: false,
  has_sorted: false,
  el: $('#bp'),

  buttons: {

    html: 'bp',

    button1: {
        id: 'get-gwas-1',
        //search term in disease_trait
         typeOfTable: 'grs',//type of table to draw from interpretome.html
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        disease: 'sbp',
        category: 'sbp',
        //type specific or multiple in the quotes if needed.
        specificOrMultiple: 'grs',
        data_table: google.visualization.arrayToDataTable([
          ['Scores', 'Population', 'Yours'],
[-11.462156, 1, 0 ], [-11.148156, 0, 0 ], [-10.834156, 3, 0 ], [-10.520156, 11, 0 ], [-10.206156, 21, 0 ], 
[-9.892156, 29, 0 ], [-9.578156, 45, 0 ], [-9.264156, 65, 0 ], [-8.950156, 77, 0 ], [-8.636156, 83, 0 ], 
[-8.322156, 109, 0 ], [-8.008156, 98, 0 ], [-7.694156, 78, 0 ], [-7.380156, 67, 0 ], [-7.066156, 37, 0 ], 
[-6.752156, 18, 0 ], [-6.438156, 8, 0 ], [-6.124156, 2, 0 ], [-5.810156, 1, 0 ], [-5.496156, 1, 0 ], 

])
        
        

    },
    //button 2
    button2: {
        id: 'get-gwas-2',
        //search term in disease_trait
        disease: 'dbp',
        category: 'dbp',
         typeOfTable: 'grs',//type of table to draw grs, pvalue, null
        geneticRiskScore: 'yes',//should the table have a running risk score column? yes/no
        //type specific or multiple in the quotes if needed.
        specificOrMultiple: 'grs',
        data_table: google.visualization.arrayToDataTable([
          ['Scores', 'Population', 'Yours'],
[-6.318992, 1, 0 ], [-6.134842, 3, 0 ], [-5.950692, 9, 0 ], [-5.766542, 13, 0 ], [-5.582392, 21, 0 ], 
[-5.398242, 29, 0 ], [-5.214092, 43, 0 ], [-5.029942, 52, 0 ], [-4.845792, 80, 0 ], [-4.661642, 78, 0 ], 
[-4.477492, 93, 0 ], [-4.293342, 99, 0 ], [-4.109192, 80, 0 ], [-3.925042, 66, 0 ], [-3.740892, 40, 0 ], 
[-3.556742, 21, 0 ], [-3.372592, 15, 0 ], [-3.188442, 10, 0 ], [-3.004292, 0, 0 ], [-2.820142, 0, 0 ], 

])
},


  },
  
  events: { 
    'click button': 'click_button'
},


  initialize: function() {
    _.bindAll(this, 'click_button');

  },
  
  render: function() {
    view_functions.render(this);
  },
    

  click_button: function(event) {

      view_functions.click_button(event, this);
  }

  });

});