$(function() {
window.FabianDisorderView = Backbone.View.extend({
  has_loaded: false,
  el: $('#fabian'),

  events: { 'click #get-gwas': 'click_disease' },

  initialize: function() {
    _.bindAll(this, 'click_disease', 'got_diseases', 'loaded');
  },
  
  render: function() {
    $.get('/media/template/fabian.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  $('button').button();
    $('#fabian-table').hide();
	  this.disease_template = $('#fabian-template').html();
	  match_style(this.el);
    $('#fabian-table').tablesorter();
    this.has_loaded = true;
  },
  
  got_diseases: function(response) {
    var self = this;
    user = get_user();
    $.each(response, function(i, v) {
      var dbsnp = user.lookup(filter_identifier(v['strongest_snp']));
      if (dbsnp != undefined){
        v['genotype'] = dbsnp.genotype;
        $('#fabian-table > tbody').append(_.template(self.disease_template, v));
      }
    });
    $('#fabian-table').show();
    $('#fabian-table').trigger('update');
    $('#looking-up').dialog('close');
  },
  
  click_disease: function(event) {
    clear_table('fabian-table');
    
    if (window.App.check_all() == false) return;
    $('#looking-up').dialog('open');
    $.get('/disease/get_gwas', {population: get_user().population, disease: 'Fabian'}, this.got_diseases);
  }
  });
});
