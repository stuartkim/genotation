$(function() {
window.BloodParameterIntroView = Backbone.View.extend({
  has_loaded: false,
  el: $('#BloodParameterIntro'),

  
  initialize: function() {
    _.bindAll(this, 'loaded');
  },
  
  render: function() {
    $.get('/media/template/BloodParameterIntro.html', this.loaded);
  },
    
  loaded: function(response) {
	  $(this.el).append(response);
	  
	  match_style(this.el);
  
    this.has_loaded = true;
  },
  
  
  });
});
