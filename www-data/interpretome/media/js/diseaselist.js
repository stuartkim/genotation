

//add to one of these to make a button (make sure you have the corresponding js and html files)


var miscList = ['selection'];

var clinicalList = ['fh', 'cystic', 'type1', 'rheumatoid', 'sickle', 'bp', 
'cvd', 'bmd2', 'ankylosing', 'disease', 'lung', 'forced', 'diabetictraits', 'schizo',
"amd", "BloodParameterIntro", "osteoarthritis", 'acl', 'achilles', 'kidney',
'bipolar', 'celiac', 'disc', 'depression', 'eso', 'graves', 'asthma', 'migraine', 'pku', 
'platelet', 'qt','vitiligo','diabetes', 'warfarin', 'pharmacogenomics'];
//put in freqtest if you want to run AJs genotyper.
var traitsList = [ 'vitamin', 'bra', 'musician', 'obesity', 'paraoxonase', 'aging', 'lean', 
'bitter', 'telomere', 'eyecolor', 'baldness','coffee','caffeine','tan','eqtl','GWAS',
'height','neandertal', 'selection'];

var ancestryList = ['PCA','painting', 'similarity', 'phasing'];

var noButtonList = ['homocysteine', 'magnesium','calcium', 'iron', 'vite','phyto', 'vitd', 'vb12', 'traits',
'BloodParameterData','tablelegends', 'clinical', 'trace', 'creatinine','esrd', 'rentran','renfun','nephropathy',
'ckd'];


//don't put stuff here, instead, use "setNickname" function below
var hasNicknameList = [];
var nicknameList = [];

//if you need an nickname, set it via the setNickname function here.
//first parameter is the name you gave earlier
//second param is the new name


//alphabetizes list
clinicaList=clinicalList.sort(function (a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
});
traitsList=traitsList.sort(function (a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
});
ancestryList=ancestryList.sort(function (a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
});

var arrayList = [miscList, clinicalList, traitsList, ancestryList, noButtonList];
setNickname('graves', 'Graves\' disease');
setNickname('amd', 'Age-related macular degeneration');
//setNickname('bmd', 'Bone mineral density');
setNickname('pku', 'Phenylketonuria');
setNickname('disc', 'Disc degeneration');
setNickname('eso', 'Esophageal cancer');
setNickname('qt', 'QT Interval');
setNickname('eqtl', 'eQTL');
setNickname('gwas', 'GWAS');
//setNickname('pca', 'PCA');
setNickname('bipolar', 'Bipolar disorder');
setNickname('BloodParameterIntro', 'Blood parameters');
setNickname('eyecolor', 'Eye Color');
setNickname('diabetictraits', 'Combined Diabetic Traits');
setNickname('forced', 'Lung Function');
setNickname('lung', 'Lung Disease');
setNickname('disease', 'All GWAS studies');
setNickname('telomere', 'Telomere Length');
setNickname('bitter', 'Bitter Taste Perception');
setNickname('lean', 'Lean Body Mass');
setNickname('aging', 'Aging and Longevity');
setNickname('ankylosing', 'Ankylosing spondylitis');
setNickname('paraoxonase', 'PON1 activity');
setNickname('bmd2', 'Bone Mineral Density GRS');
setNickname('cvd', 'Cardiovascular Disease GRS');
setNickname('obesity', 'Obesity GRS');
setNickname('bp', 'Blood Pressure/CAD/Stroke GRS');
setNickname('sickle', 'Sickle Cell Trait');
setNickname('musician', 'Musician\'s dystonia');
setNickname('rheumatoid', 'Rheumatoid Arthritis');
setNickname('type1', 'Type 1 Diabetes');
setNickname('ckd', 'Chronic Kidney Disease GRS');
setNickname('celiac', 'Celiac Disease GRS and GWAS');
setNickname('cystic', 'Cystic Fibrosis');
setNickname('fh', 'Familial Hypercholesterolemia');
setNickname('bra', 'Bra Size GRS');
setNickname('vitamin', 'Vitamins and Minerals');
setNickname('acl', 'ACL rupture');
setNickname('achilles', 'Achilles tendonopathy');
setNickname('platelet', 'Platelet count');
setNickname('tan', 'tanning');
setNickname('kidney', 'Kidney Diseases');
setNickname('schizo', 'Schizophrenia GRS');


  //for storing functions
  var miscFuncs = [];
  var clinicalFuncs = [];
  var traitsFuncs = [];
  var ancestryFuncs = [];
  var noButtonFuncs= [];


//adds nickname to its arrays
function setNickname(originalName, newName)
{


  for(var i = 0; i < arrayList.length; i++)
      {
        for(var j = 0; j < arrayList[i].length; j++)
        {

          if (arrayList[i][j]===originalName)
          {

            hasNicknameList.push(originalName);
            nicknameList.push(newName);
            break;
          }
        }

       }
       
}




//capitalizes the thing, removes underscores if second param is true

function capitalize(string, removeUnderscores)
{
  var newString = "";
  if (string.length>1)
  {
    for(var i =1; i<string.length; i++)
   {
      if (removeUnderscores===true && string.charAt(i)=="_")
      {
         newString+=" ";
      }
      else{
      newString+=string.charAt(i);
    }
   }
    return string.charAt(0).toUpperCase()+newString;
  }
}

function goTo(location)
{
		location = '#' + location;
		$('#tabs').tabs('select', location);
}
